import 'package:get/get.dart';

import '../modules/browsers/bindings/browsers_binding.dart';
import '../modules/browsers/views/browsers_view.dart';
import '../modules/chatgpt/bindings/chatgpt_binding.dart';
import '../modules/chatgpt/views/chatgpt_view.dart';
import '../modules/create_wallet/bindings/create_wallet_binding.dart';
import '../modules/create_wallet/views/create_wallet_view.dart';
import '../modules/createnft/bindings/createnft_binding.dart';
import '../modules/createnft/views/createnft_view.dart';
import '../modules/detail_nft/bindings/detail_nft_binding.dart';
import '../modules/detail_nft/views/detail_nft_view.dart';
import '../modules/detail_token/bindings/detail_token_binding.dart';
import '../modules/detail_token/views/detail_token_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/import_wallet/bindings/import_wallet_binding.dart';
import '../modules/import_wallet/views/import_wallet_view.dart';
import '../modules/lesson/bindings/lesson_binding.dart';
import '../modules/lesson/views/lesson_view.dart';
import '../modules/news/bindings/news_binding.dart';
import '../modules/news/views/news_view.dart';
import '../modules/onboarding/bindings/onboarding_binding.dart';
import '../modules/onboarding/views/onboarding_view.dart';
import '../modules/password/bindings/password_binding.dart';
import '../modules/password/views/password_view.dart';
import '../modules/receivetx/bindings/receivetx_binding.dart';
import '../modules/receivetx/views/receivetx_view.dart';
import '../modules/sendnft/bindings/sendnft_binding.dart';
import '../modules/sendnft/views/sendnft_view.dart';
import '../modules/sendtx/bindings/sendtx_binding.dart';
import '../modules/sendtx/views/sendtx_view.dart';
import '../modules/setup_biometric/bindings/setup_biometric_binding.dart';
import '../modules/setup_biometric/views/setup_biometric_view.dart';
import '../modules/setup_password/bindings/setup_password_binding.dart';
import '../modules/setup_password/views/setup_password_view.dart';
import '../modules/swap/bindings/swap_binding.dart';
import '../modules/swap/views/swap_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.ONBOARDING;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.ONBOARDING,
      page: () => OnboardingView(),
      binding: OnboardingBinding(),
    ),
    GetPage(
      name: _Paths.CREATE_WALLET,
      page: () => CreateWalletView(),
      binding: CreateWalletBinding(),
    ),
    GetPage(
      name: _Paths.IMPORT_WALLET,
      page: () => ImportWalletView(),
      binding: ImportWalletBinding(),
    ),
    GetPage(
      name: _Paths.PASSWORD,
      page: () => PasswordView(),
      binding: PasswordBinding(),
    ),
    GetPage(
      name: _Paths.SETUP_PASSWORD,
      page: () => const SetupPasswordView(),
      binding: SetupPasswordBinding(),
    ),
    GetPage(
      name: _Paths.SETUP_BIOMETRIC,
      page: () => const SetupBiometricView(),
      binding: SetupBiometricBinding(),
    ),
    GetPage(
      name: _Paths.SENDTX,
      page: () => SendtxView(),
      binding: SendtxBinding(),
    ),
    GetPage(
      name: _Paths.RECEIVETX,
      page: () => ReceivetxView(),
      binding: ReceivetxBinding(),
    ),
    GetPage(
      name: _Paths.NEWS,
      page: () => const NewsView(),
      binding: NewsBinding(),
    ),
    GetPage(
      name: _Paths.LESSON,
      page: () => LessonView(),
      binding: LessonBinding(),
    ),
    GetPage(
      name: _Paths.CHATGPT,
      page: () => const ChatgptView(),
      binding: ChatgptBinding(),
    ),
    GetPage(
      name: _Paths.BROWSERS,
      page: () => BrowsersView(),
      binding: BrowsersBinding(),
    ),
    GetPage(
      name: _Paths.CREATENFT,
      page: () => const CreatenftView(),
      binding: CreatenftBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_TOKEN,
      page: () => DetailTokenView(),
      binding: DetailTokenBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_NFT,
      page: () => DetailNftView(),
      binding: DetailNftBinding(),
    ),
    GetPage(
      name: _Paths.SENDNFT,
      page: () => SendnftView(),
      binding: SendnftBinding(),
    ),
    GetPage(
      name: _Paths.SWAP,
      page: () => const SwapView(),
      binding: SwapBinding(),
    ),
  ];
}

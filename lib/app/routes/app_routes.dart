part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const ONBOARDING = _Paths.ONBOARDING;
  static const CREATE_WALLET = _Paths.CREATE_WALLET;
  static const IMPORT_WALLET = _Paths.IMPORT_WALLET;
  static const PASSWORD = _Paths.PASSWORD;
  static const SETUP_PASSWORD = _Paths.SETUP_PASSWORD;
  static const SETUP_BIOMETRIC = _Paths.SETUP_BIOMETRIC;
  static const SENDTX = _Paths.SENDTX;
  static const RECEIVETX = _Paths.RECEIVETX;
  static const NEWS = _Paths.NEWS;
  static const LESSON = _Paths.LESSON;
  static const CHATGPT = _Paths.CHATGPT;
  static const BROWSERS = _Paths.BROWSERS;
  static const CREATENFT = _Paths.CREATENFT;
  static const DETAIL_TOKEN = _Paths.DETAIL_TOKEN;
  static const DETAIL_NFT = _Paths.DETAIL_NFT;
  static const SENDNFT = _Paths.SENDNFT;
  static const SWAP = _Paths.SWAP;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const ONBOARDING = '/onboarding';
  static const CREATE_WALLET = '/create-wallet';
  static const IMPORT_WALLET = '/import-wallet';
  static const PASSWORD = '/password';
  static const SETUP_PASSWORD = '/setup-password';
  static const SETUP_BIOMETRIC = '/setup-biometric';
  static const SENDTX = '/sendtx';
  static const RECEIVETX = '/receivetx';
  static const NEWS = '/news';
  static const LESSON = '/lesson';
  static const CHATGPT = '/chatgpt';
  static const BROWSERS = '/browsers';
  static const CREATENFT = '/createnft';
  static const DETAIL_TOKEN = '/detail-token';
  static const DETAIL_NFT = '/detail-nft';
  static const SENDNFT = '/sendnft';
  static const SWAP = '/swap';
}

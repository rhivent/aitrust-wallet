// import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:walletdapps/app/controllers/global_controller.dart';
import 'package:walletdapps/app/data/listnetworkblockchain_model.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/config/constantsData.dart';
import 'package:walletdapps/contracts/ERC721.g.dart';
import 'package:walletdapps/contracts/Erc20.g.dart';
import 'package:walletdapps/contracts/NFT1155.g.dart';
import 'package:walletdapps/contracts/PancakeSwap.g.dart';
import 'package:walletdapps/services/wallet.dart';
import 'package:web3dart/web3dart.dart';

class WalletController extends GetxController {
  final globalC = Get.find<GlobalController>();
  var constantInstance =
      BlockchainData.fromJsonList(ConstantsClass.networkData);
  var networkList =
      Listnetworkblockchain.fromJsonList(ConstantsClass.listStringNetwork);
  late Web3Client _ethClient;

  final _balance = ''.obs;
  final _dataawal = <String, dynamic>{}.obs;
  final httpsRPC = ''.obs;
  final wssRPC = ''.obs;
  var abi721Code = '';
  var abi1155Code = '';
  final selectedChainId = 0.obs;
  final selectedNetwork = <String, dynamic>{}.obs;
  final tokensByAddress = [].obs;
  final nftsByAddress = [].obs;

  late Erc20 _contractERC20;
  BlockchainData selectedBlockchain = BlockchainData(
    httpsRpc: '',
    wssRpc: '',
    chainId: 0,
    chainIdHex: "0x61",
    explorerUrl: '',
    baseApi: '',
    logo: '',
    nativeCoin: '',
    apikKey: '',
    tokens: [],
  );

  get datawal => _dataawal;
  String get balance => _balance.value;

  @override
  void onInit() {
    selectedChainId.value = GetStorage().read('chainId') ?? 97;
    var filterNetwork = networkList.firstWhere(
        (element) => element.value == selectedChainId.value.toString());
    selectedNetwork.value = {
      "name": filterNetwork.name,
      "value": filterNetwork.value
    };
    selectedBlockchain = constantInstance
        .firstWhere((element) => element.chainId == selectedChainId.value);
    httpsRPC.value = constantInstance
        .firstWhere((element) => element.chainId == selectedChainId.value)
        .httpsRpc;

    wssRPC.value = constantInstance
        .firstWhere((element) => element.chainId == selectedChainId.value)
        .wssRpc;
    _ethClient = Web3Client(httpsRPC.value, http.Client());

    // print(GetStorage().read('wallet'));

    if (GetStorage().read('wallet') == null) {
      GetStorage().writeIfNull("wallet", {
        "seed": "",
        "pv": "",
        "pub": "",
      });
      _dataawal.value = {
        "seed": "",
        "pv": "",
        "pub": "",
      };
    } else {
      var dt = GetStorage().read("wallet") as Map<String, dynamic>;
      _dataawal.value = {
        "seed": dt["seed"],
        "pv": dt["pv"],
        "pub": dt["pub"],
      };
      checkBalance();
    }

    super.onInit();
  }

  void changeBlockchain(int? chainId) async {
    GetStorage().write("chainId", chainId);
    selectedChainId.value = chainId ?? 97;
    var filterNetwork = networkList
        .firstWhere((element) => element.value == chainId.toString());
    selectedNetwork.value = {
      "name": filterNetwork.name,
      "value": filterNetwork.value
    };

    selectedBlockchain =
        constantInstance.firstWhere((element) => element.chainId == chainId);
    httpsRPC.value = constantInstance
        .firstWhere((element) => element.chainId == chainId)
        .httpsRpc;

    wssRPC.value = constantInstance
        .firstWhere((element) => element.chainId == chainId)
        .wssRpc;

    _ethClient = Web3Client(httpsRPC.value, http.Client());
    await checkBalance();
  }

  Future<Erc20> loadContract(String address) async {
    _contractERC20 = Erc20(
      address: EthereumAddress.fromHex(address),
      client: _ethClient,
      chainId: selectedChainId.value,
    );
    return _contractERC20;
  }

  Future<BigInt> allowanceToken(
      String owneraddress, String spenderaddress, String tokenaddress) async {
    try {
      var contract = await loadContract(tokenaddress);
      BigInt result = await contract.allowance(
        EthereumAddress.fromHex(owneraddress),
        EthereumAddress.fromHex(spenderaddress),
      );
      return result;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<BigInt> balanceToken(String tokenaddress) async {
    try {
      var contract = await loadContract(tokenaddress);
      BigInt result = await contract.balanceOf(
        EthereumAddress.fromHex(datawal['pub'].toString()),
      );
      return result;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<String> transferToken(
      String recepient, double amount, String tokenaddress) async {
    try {
      var contract = await loadContract(tokenaddress);
      if (datawal['pv'].toString() != '') {
        EthPrivateKey credentials = EthPrivateKey.fromHex(
          datawal['pv'].toString(),
        );
        String transfer = await contract.transfer(
          EthereumAddress.fromHex(recepient),
          BigInt.from(amount * pow(10, 18)),
          credentials: credentials,
        );
        return transfer;
      } else {
        throw Exception("privatekey null");
      }
    } catch (e) {
      throw e.toString();
    }
  }

  Future<void> setDataImport(String privateKey) async {
    globalC.setLoading(true);
    try {
      WalletAddress wallet = WalletAddress();
      final pub = wallet.getPublicKey(privateKey);

      await GetStorage().write('wallet', {
        "seed": '-',
        "pv": privateKey,
        "pub": pub.toString(),
      });
      _dataawal.value = {
        "seed": '-',
        "pv": privateKey,
        "pub": pub.toString(),
      };
      await checkBalance();
      globalC.setLoading(false);
    } catch (e) {
      // if (kDebugMode) {
      // print(e);
      // }
      Get.snackbar("Error", "Invalid Private Key ");
      globalC.setLoading(false);
    }
  }

  Future<void> createWallet() async {
    globalC.setLoading(true);
    try {
      // Future.delayed(Duration(seconds: 5), () async {
      // });
      WalletAddress wallet = WalletAddress();
      final seed = wallet.generateMnemonic();
      final priveky = await wallet.getPrivateKey(seed);
      final pubky = wallet.getPublicKey(priveky);
      _dataawal.value = {
        "seed": seed,
        "pv": priveky,
        "pub": pubky.toString(),
      };

      await GetStorage().write('wallet', {
        "seed": seed,
        "pv": priveky,
        "pub": pubky.toString(),
      });
      await checkBalance();
      if (kDebugMode) {
        print(seed);
        print(priveky);
        print(pubky);
      }
      globalC.setLoading(false);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      globalC.setLoading(false);
    }
  }

  Future<void> checkBalance() async {
    globalC.setLoading(true);
    try {
      // print('checkBalance');
      if (datawal['pv'].toString() != '') {
        // var credentials = EthPrivateKey.fromHex(datawal['pv']);
        // print(address.hexEip55);
        var bl = await _ethClient
            .getBalance(EthereumAddress.fromHex(datawal['pub']));
        _balance.value = bl.getValueInUnit(EtherUnit.ether).toString();
        await getAllTokens();
      } else {
        _balance.value = "0";
        globalC.setLoading(false);
      }
      globalC.setLoading(false);
    } catch (e) {
      Get.defaultDialog(content: Text(e.toString()));
      if (kDebugMode) {
        print(e);
      }
      _balance.value = "0";
      globalC.setLoading(false);
    }
  }

  Future<void> checkUnit() async {
    try {
      if (kDebugMode) {
        print('checkUnit');
        if (datawal['pv'].toString() != '') {
          print(
            EtherAmount.fromUnitAndValue(EtherUnit.ether, 1),
          ); // dari ether ke wei, params 2
        }
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  Future<String> transferNativeCoin({
    required String recepient,
    required double amount,
    int chainId = 97,
  }) async {
    try {
      if (datawal['pv'].toString() != '') {
        EthPrivateKey credentials =
            EthPrivateKey.fromHex(datawal['pv'].toString());
        String transfer = await _ethClient.sendTransaction(
          credentials,
          Transaction(
            to: EthereumAddress.fromHex(recepient),
            value: EtherAmount.inWei(BigInt.from(amount * pow(10, 18))),
          ),
          chainId: chainId,
        );
        return transfer;
      } else {
        throw Exception("privatekey null");
      }
    } catch (e) {
      throw e.toString();
    }
  }

  Future<void> getAllTokens() async {
    try {
      //https://deep-index.moralis.io/api/v2/0x8C1A5b5d41CcA3C4068bCDbbE3fBe770b8B2AEdc/erc20?chain=0x61
      Uri apiUrl = Uri.parse(
          'https://deep-index.moralis.io/api/v2/${datawal['pub'].toString()}/erc20?chain=0x${selectedChainId.value.toRadixString(16)}');
      HttpClient client = HttpClient();
      client.autoUncompress = true;

      final HttpClientRequest request = await client.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      request.headers.set("X-API-Key", ConstantsClass.moralisAPIKey);
      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final List data = json.decode(content);
      /*
        [
          {
            "token_address": "0xd922d4cd21dc1f62d8d8ae0db3039cc0f36fa83c",
            "name": "Tether USD",
            "symbol": "USDT",
            "logo": null,
            "thumbnail": null,
            "decimals": 18,
            "balance": "129898598999000000000000000"
          },
        ];
      */
      tokensByAddress.value = data;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<void> getAllNFTs() async {
    try {
      Uri apiUrl = Uri.parse(
          'https://deep-index.moralis.io/api/v2/${datawal['pub'].toString()}/nft?chain=0x${selectedChainId.value.toRadixString(16)}&format=decimal&normalizeMetadata=true');
      HttpClient client = HttpClient();
      client.autoUncompress = true;

      final HttpClientRequest request = await client.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      request.headers.set("X-API-Key", ConstantsClass.moralisAPIKey);

      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final data = json.decode(content) as Map<String, dynamic>;
      final result = data['result'];
      // print(result);
      /*
        [
          {
            "token_address": "0xe96c3a55cbf804c98027621285e07fbc5f6f7474",
            "token_id": "4",
            "owner_of": "0xbcefc6b0134c34d4de6d3956b028dfa2f3c79fdb",
            "block_number": "24150337",
            "block_number_minted": "24150337",
            "token_hash": "52108cbb9dffb9c5449effcf4034f025",
            "amount": "45",
            "contract_type": "ERC1155",
            "name": null,
            "symbol": null,
            "token_uri": "https://bafyreifvvlan6n2pzocco5cgwljqq7v6wd5r7efbvna6c5kl6ga3nlwxmy.ipfs.nftstorage.link/metadata.json",
            "metadata": "{\"name\":\"Pitbull Tickets\",\"description\":\"Ticket live music PITBULL ALL NIGHT PARTY\",\"category\":\"ticket\",\"nfttype\":\"erc1155\",\"amountnft\":50,\"image\":\"ipfs://bafybeib3mkexemck4hyap4xhqsqabqbnp7mngpvkd364uck6xvkbyondqu/pitbulltickets.jpg\"}",
            "last_token_uri_sync": "2022-10-30T08:30:43.247Z",
            "last_metadata_sync": "2022-11-14T07:16:55.408Z",
            "minter_address": "ERC1155 tokens don't have a single minter",
            "normalized_metadata": {
              "name": "Pitbull Tickets",
              "description": "Ticket live music PITBULL ALL NIGHT PARTY",
              "animation_url": null,
              "external_link": null,
              "image": "ipfs://bafybeib3mkexemck4hyap4xhqsqabqbnp7mngpvkd364uck6xvkbyondqu/pitbulltickets.jpg",
              "attributes": []
            }
          },
        ];
      */
      nftsByAddress.value = result;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<dynamic> getMetadaToken(String address) async {
    try {
      Uri apiUrl = Uri.parse(
          "https://deep-index.moralis.io/api/v2/erc20/metadata?chain=0x${selectedChainId.value.toRadixString(16)}&addresses%5B0%5D=$address");
      HttpClient client = HttpClient();
      client.autoUncompress = true;

      final HttpClientRequest request = await client.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      request.headers.set("X-API-Key", ConstantsClass.moralisAPIKey);
      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final data = json.decode(content);
      /* data contain: 
      [
        {
          "address": "0x84b9b910527ad5c03a9ca831909e21e236ea7b06",
          "name": "ChainLink Token",
          "symbol": "LINK",
          "logo": null,
          "logo_hash": null,
          "thumbnail": null,
          "decimals": "18",
          "block_number": "206106",
          "validated": 1,
          "created_at": "2022-01-20T10:40:46.684Z"
        }
      ]
      */
      final result = data[0];
      return result;
    } catch (e) {
      // print('getMetadaToken');
      throw e.toString();
    }
  }

  Future<void> addTokenList(String address) async {
    try {
      // get data from moralis api
      var metadt = await getMetadaToken(address) as Map<String, dynamic>;
      //check token is has been added or not
      var containsToken = tokensByAddress.where(
        (element) => element['symbol'] == metadt["symbol"],
      );
      if (containsToken.isNotEmpty) {
        throw Exception("Token ${metadt['symbol']} already added");
      }

      // check balance token for current users
      var bigIntBalance = await balanceToken(address);
      var doubleBalance =
          bigIntBalance.toDouble() / pow(10, int.parse(metadt["decimals"]));
      tokensByAddress.add({
        "token_address": address,
        "name": metadt["name"],
        "symbol": metadt["symbol"],
        "balance": doubleBalance.toString(),
        "logo": null,
        "thumbnail": null,
        "decimals": int.parse(metadt["decimals"]),
      });
    } catch (e) {
      throw e.toString();
    }
  }

  Future<void> cleardt() async {
    try {
      _dataawal.value = {
        "seed": "",
        "pv": "",
        "pub": "",
      };
      await GetStorage().erase();
      Get.snackbar("Cleared !", "Success Clear Data Storage");
      Get.offAllNamed(Routes.ONBOARDING);
    } catch (e) {
      // print(e);
      Get.snackbar("ERROR", "ERROR Clear Data Storage: ${e.toString()}");
      globalC.setLoading(false);
    }
  }

  Future<String> sendNFT(recepientAddress, amount, datanft) async {
    EthPrivateKey credentials = EthPrivateKey.fromHex(datawal['pv'].toString());
    try {
      var txhash = "";
      if (datanft['contract_type'] == "ERC1155") {
        final contractAbi = NFT1155(
          address: EthereumAddress.fromHex(datanft['token_address']),
          client: _ethClient,
          chainId: selectedChainId.value,
        );

        txhash = await contractAbi.safeTransferFrom(
          EthereumAddress.fromHex(datawal['pub']),
          EthereumAddress.fromHex(recepientAddress),
          BigInt.from(int.parse(datanft['token_id'])),
          BigInt.from(int.parse(amount)),
          Uint8List(1),
          credentials: credentials,
        );
      } else {
        final contractAbi = ERC721(
          address: EthereumAddress.fromHex(datanft['token_address']),
          client: _ethClient,
          chainId: selectedChainId.value,
        );
        txhash = await contractAbi.safeTransferFrom(
          EthereumAddress.fromHex(datawal['pub']),
          EthereumAddress.fromHex(recepientAddress),
          BigInt.from(int.parse(datanft['token_id'])),
          credentials: credentials,
        );
      }
      return txhash;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<String> swapWallet(bool isOriginNative, String routerAddr,
      double amount, String originAddr, String resultAddr) async {
    try {
      EthPrivateKey credentials =
          EthPrivateKey.fromHex(datawal['pv'].toString());
      final contractAbi = PancakeSwap(
        address: EthereumAddress.fromHex(routerAddr),
        client: _ethClient,
        chainId: selectedChainId.value,
      );
      var contractPair = await contractAbi.factory();
      var txhash = "";
      var deadline =
          DateTime.now().add(const Duration(days: 365)).millisecondsSinceEpoch /
              1000;
      if (isOriginNative) {
        // swap native coin to token
        txhash = await contractAbi.swapExactETHForTokens(
          BigInt.from(0), //amountOutMin
          [
            EthereumAddress.fromHex(originAddr),
            EthereumAddress.fromHex(resultAddr),
          ],
          EthereumAddress.fromHex(datawal['pub']),
          BigInt.from(deadline),
          credentials: credentials,
          transaction: Transaction(
            from: EthereumAddress.fromHex(datawal['pub']),
            value: EtherAmount.fromUnitAndValue(
              EtherUnit.wei,
              BigInt.from(amount * pow(10, 18)),
            ),
          ),
        );
      } else {
        final contractERC20OriginAbi = Erc20(
          address: EthereumAddress.fromHex(originAddr),
          client: _ethClient,
          chainId: selectedChainId.value,
        );
        var isapproved = await contractERC20OriginAbi.allowance(
            EthereumAddress.fromHex(datawal['pub']), contractPair);
        /*
          print(BigInt.from(1).compareTo(BigInt.from(2))); // => -1 lessthan
          print(BigInt.from(2).compareTo(BigInt.from(1))); // => 1 greater than 
          print(BigInt.from(1).compareTo(BigInt.from(1))); // => 0 equal
        */
        if (isapproved.compareTo(BigInt.from(amount * pow(10, 18))) > 0) {
          // print('masuk 574');
        } else {
          // print('masuk 576');
          // make approval contract swap pair
          await contractERC20OriginAbi.approve(
            contractPair,
            BigInt.from(amount * pow(10, 18) * pow(10, 18)),
            credentials: credentials,
          );
          await contractERC20OriginAbi.approve(
            EthereumAddress.fromHex(routerAddr),
            BigInt.from(amount * pow(10, 18) * pow(10, 18)),
            credentials: credentials,
          );
        }

        // final contractERC20ResultAbi = Erc20(
        //   address: EthereumAddress.fromHex(resultAddr),
        //   client: _ethClient,
        //   chainId: selectedChainId.value,
        // );

        // var isapprovedResult = await contractERC20ResultAbi.allowance(
        //     EthereumAddress.fromHex(datawal['pub']),
        //     EthereumAddress.fromHex(routerAddr));
        // print(isapprovedResult.compareTo((BigInt.from(amount * pow(10, 18)))));
        // if (isapprovedResult.compareTo((BigInt.from(amount * pow(10, 18)))) ==
        //     1) {
        //   print('masul 603');
        //   //
        // } else {
        //   print('masuk 594');
        //   // make approval
        //   await contractERC20ResultAbi.approve(
        //     EthereumAddress.fromHex(routerAddr),
        //     BigInt.from(amount * pow(10, 18)),
        //     credentials: credentials,
        //   );
        // }

        // swap token to native coin
        txhash = await contractAbi.swapExactTokensForETH(
          BigInt.from(amount * pow(10, 18)), //amountIn
          BigInt.from(0), //amountOutMin
          [
            EthereumAddress.fromHex(originAddr),
            EthereumAddress.fromHex(resultAddr),
          ],
          EthereumAddress.fromHex(datawal['pub']),
          BigInt.from(deadline),
          credentials: credentials,
        );
      }
      return txhash;
    } catch (e) {
      print(e.toString());
      throw e.toString();
    }
  }

  @override
  void onClose() async {
    await _ethClient.dispose();
    super.onClose();
  }
}

// import 'package:flutter/foundation.dart';
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:uni_links/uni_links.dart';

class GlobalController extends GetxController {
  final _theme = false.obs;
  get theme => _theme.value;
  final _loadingGlobal = false.obs;
  get loadingGlobal => _loadingGlobal.value;
  final navIndex = 2.obs;

  Uri? _initialURI;
  Uri? _currentURI;
  Object? _errDeeplink;
  get deeplinkApp => _initialURI;
  get deeplinkWeb => _currentURI;
  get deeplinkErr => _errDeeplink;

  StreamSubscription? _streamSubscription;
  bool _initialURILinkHandled = false;
  @override
  void onInit() {
    if (GetStorage().read('theme') != null) {
      _theme.value = GetStorage().read('theme');
    }
    super.onInit();
  }

  @override
  void onReady() {
    _initURIHandler();
    _incomingLinkHandler();
    super.onReady();
  }

  void settheme(bool dt) async {
    _loadingGlobal.value = true;
    try {
      await GetStorage().write("theme", dt);
      _theme.value = dt;
      _loadingGlobal.value = false;
    } catch (e) {
      _loadingGlobal.value = false;
      // print(e);
    }
  }

  void setLoading(bool ld) {
    _loadingGlobal.value = ld;
  }

  void setnavIndex(int indx) {
    navIndex.value = indx;
  }

  Future<void> _initURIHandler() async {
    if (!_initialURILinkHandled) {
      _initialURILinkHandled = true;
      // Get.snackbar("Invoked _initURIHandler", "Coba Invoked _initURIHandler");
      try {
        final initialURI = await getInitialUri();
        // Use the initialURI and warn the user if it is not correct,
        // but keep in mind it could be `null`.
        if (initialURI != null) {
          // Get.snackbar("masuk keuri", initialURI.toString());
          debugPrint("Initial URI received $initialURI");
          _initialURI = initialURI;
        } else {
          // Get.snackbar("ERROR", "Empty data uri link");
          debugPrint("Null Initial URI received");
        }
      } on PlatformException {
        // Platform messages may fail, so we use a try/catch PlatformException.
        // Handle exception by warning the user their action did not succeed
        debugPrint("Failed to receive initial uri");
      } on FormatException catch (err) {
        debugPrint('Malformed Initial URI received');
        _errDeeplink = err;
      }
    }
  }

  /// Handle incoming links - the ones that the app will receive from the OS
  /// while already started.
  void _incomingLinkHandler() {
    if (!kIsWeb) {
      // It will handle app links while the app is already started - be it in
      // the foreground or in the background.
      _streamSubscription = uriLinkStream.listen((Uri? uri) {
        debugPrint('Received URI: $uri');
        _currentURI = uri;
        _errDeeplink = null;
        print(_currentURI);
      }, onError: (Object err) {
        debugPrint('Error occurred: $err');
        _currentURI = null;
        if (err is FormatException) {
          _errDeeplink = err;
        } else {
          _errDeeplink = null;
        }
      });
    }
  }

  @override
  void onClose() {
    _streamSubscription?.cancel();
    super.onClose();
  }
}

class Listnetworkblockchain {
  String? name;
  String? value;

  Listnetworkblockchain({this.name, this.value});

  Listnetworkblockchain.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['value'] = value;
    return data;
  }

  static List<Listnetworkblockchain> fromJsonList(List list) {
    if (list.isEmpty) return List<Listnetworkblockchain>.empty();
    return list.map((item) => Listnetworkblockchain.fromJson(item)).toList();
  }
}

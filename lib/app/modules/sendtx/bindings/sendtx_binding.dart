import 'package:get/get.dart';

import '../controllers/sendtx_controller.dart';

class SendtxBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SendtxController>(
      () => SendtxController(),
    );
  }
}

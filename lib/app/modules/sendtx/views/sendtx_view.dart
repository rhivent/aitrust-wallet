import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/sendtx_controller.dart';

class SendtxView extends GetView<SendtxController> {
  final walletC = Get.find<WalletController>();

  SendtxView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Send'),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        children: [
          const SizedBox(
            height: 32,
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Choose Token",
              ),
            ),
          ),
          DropdownSearch(
            selectedItem: Get.arguments ??
                {
                  "token_address": "",
                  "name": walletC.selectedBlockchain.nativeCoin,
                  "symbol": "BNB",
                  "logo": walletC.selectedBlockchain.logo,
                  "thumbnail": walletC.selectedBlockchain.logo,
                  "decimals": 18,
                  "balance": walletC.balance,
                },
            popupProps: const PopupProps.menu(
              showSearchBox: true,
            ),
            dropdownDecoratorProps: const DropDownDecoratorProps(
              dropdownSearchDecoration: InputDecoration(
                labelText: "Tokens",
                hintText: "Cari Token",
                border: InputBorder.none,
                filled: true,
              ),
            ),
            itemAsString: (u) {
              return u["name"] as String;
            },
            items: [
              {
                "token_address": "",
                "name": walletC.selectedBlockchain.nativeCoin,
                "symbol": "BNB",
                "logo": walletC.selectedBlockchain.logo,
                "thumbnail": walletC.selectedBlockchain.logo,
                "decimals": 18,
                "balance": walletC.balance,
              },
              ...walletC.tokensByAddress
            ],
            onChanged: (val) {
              var tokenDt = val["token_address"] as String;
              if (val != null) {
                controller.tokenAddress.value = tokenDt;
                // print(controller.tokenAddress.value);
              } else {
                // print("nothing");
                controller.tokenAddress.value = "";
              }
            },
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Recepient Address",
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.zero,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: TextField(
                controller: controller.recepientC,
                autocorrect: false,
                autofocus: true,
                maxLines: 1, //or null
                decoration: InputDecoration(
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.qr_code_scanner),
                    onPressed: () {
                      scanQR();
                    },
                  ),
                  hintText: "Enter recepient address here",
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Amount",
              ),
            ),
          ),
          Card(
            elevation: 0,
            margin: EdgeInsets.zero,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 13),
              child: TextField(
                keyboardType: TextInputType.number,
                controller: controller.amountC,
                autocorrect: false,
                maxLines: 1, //or null
                decoration: const InputDecoration.collapsed(
                  hintText: "Enter amount ex. 0.1",
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            "Please check again receipent address !",
            style: TextStyle(
              color: Colors.red[300],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Obx(
            () => controller.loading.value
                ? const Center(child: CircularProgressIndicator())
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      shape: const StadiumBorder(),
                      padding: const EdgeInsets.symmetric(
                        vertical: 13.0,
                      ),
                      backgroundColor: Colors.blue,
                    ),
                    onPressed: () async {
                      Get.defaultDialog(
                        title: "Confirmation",
                        content: const Text("Are you sure ?"),
                        onCancel: () {},
                        confirmTextColor: Colors.white,
                        onConfirm: () async {
                          try {
                            if (controller.recepientC.text == "") {
                              throw "Required recepient address !";
                            }
                            if (controller.amountC.text == "") {
                              throw "Amount to transfer must be fill !";
                            }
                            controller.loading.value = true;
                            Get.back();
                            String tx;
                            if (controller.tokenAddress.value != "") {
                              tx = await walletC.transferToken(
                                  controller.recepientC.text,
                                  double.tryParse(controller.amountC.text) ??
                                      0.0,
                                  controller.tokenAddress.value);
                            } else {
                              tx = await walletC.transferNativeCoin(
                                recepient: controller.recepientC.text,
                                amount:
                                    double.tryParse(controller.amountC.text) ??
                                        0.0,
                              );
                            }
                            tx =
                                "${walletC.selectedBlockchain.explorerUrl}tx/$tx";
                            Clipboard.setData(ClipboardData(text: tx));
                            Get.snackbar(
                              "Success",
                              "Tx Hash: $tx has been COPIED !",
                              duration: const Duration(seconds: 10),
                            );
                            Get.offAllNamed(Routes.HOME);
                          } catch (e) {
                            controller.loading.value = false;
                            Get.back();
                            Get.snackbar("Error", e.toString());
                          }
                        },
                      );
                    },
                    child: const Text(
                      "SEND",
                      style: TextStyle(
                        height: 1.4,
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
          )
        ],
      ),
    );
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666',
        'Cancel',
        true,
        ScanMode.QR,
      );
      controller.scanQrResult.value = barcodeScanRes;
      controller.recepientC.text = barcodeScanRes;
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SendtxController extends GetxController {
  late TextEditingController recepientC;
  late TextEditingController amountC;
  final loading = false.obs;
  final tokenAddress = "".obs;
  final scanQrResult = "".obs;

  @override
  void onInit() {
    var arguments = Get.arguments ??
        {
          "token_address": "",
          "symbol": "BNB",
          "decimals": 18,
        };
    //address to test : 0x8C1A5b5d41CcA3C4068bCDbbE3fBe770b8B2AEdc
    amountC = TextEditingController(text: "");
    recepientC = TextEditingController(text: "");
    tokenAddress.value = arguments["token_address"];
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
    recepientC.dispose();
    amountC.dispose();
  }
}

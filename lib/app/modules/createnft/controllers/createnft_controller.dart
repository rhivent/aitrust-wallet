import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:io';

import 'package:walletdapps/config/constantsData.dart';

class CreatenftController extends GetxController {
  final textController = TextEditingController(text: "");
  Uint8List imageData = Uint8List.fromList([0]);

  List<int> extractUint8List(Uint8List source) {
    List<int> extractedList = [];
    for (int x in imageData) {
      extractedList.add(x);
    }
    return extractedList;
  }

  late List<int> imageListInt = extractUint8List(imageData);
  final imageOutput = "".obs;
  final isLoading = false.obs; // Add this line
  final loadingDownload = false.obs;

  void generateImg() async {
    try {
      isLoading.toggle(); // true
      // get UUID and prediction
      final response = await http.post(
        Uri.parse(
            "https://replicate.com/api/models/stability-ai/stable-diffusion/versions/db21e45d3f7023abc2a46ee38a23973f6dce16bb082a930b0c49861f96d1e5bf/predictions"),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({
          "inputs": {
            "prompt": textController.text,
            "scheduler": "DPMSolverMultistep",
            "num_outputs": 1,
            "guidance_scale": 7.5,
            "image_dimensions": "768x768",
            "negative_prompt": "porn",
            "num_inference_steps": 50
          }
        }),
      );
      if (response.statusCode != 201) {
        throw response.toString();
      }
      final data = json.decode(response.body);
      // get image output
      final getImg = await http.get(
        Uri.parse(
            "https://replicate.com/api/models/stability-ai/stable-diffusion/versions/db21e45d3f7023abc2a46ee38a23973f6dce16bb082a930b0c49861f96d1e5bf/predictions/${data["uuid"].toString()}"),
        headers: {
          'Content-Type': 'application/json',
        },
      );
      final dataJson = json.decode(getImg.body);
      if (dataJson["prediction"]["status"] == "processing") {
        await Future.delayed(const Duration(seconds: 5));
        var processDtImg = await processImg(data);
        imageOutput.value = processDtImg["prediction"]["output"][0];
      }
      isLoading.toggle(); // false
    } catch (e) {
      isLoading.toggle(); // false
      throw e.toString();
    }
  }

  Future<dynamic> processImg(dynamic data) async {
    final getImg = await http.get(
      Uri.parse(
          "https://replicate.com/api/models/stability-ai/stable-diffusion/versions/db21e45d3f7023abc2a46ee38a23973f6dce16bb082a930b0c49861f96d1e5bf/predictions/${data["uuid"].toString()}"),
      headers: {
        'Content-Type': 'application/json',
      },
    );
    return json.decode(getImg.body);
  }

  void convertTextToImage() async {
    try {
      isLoading.toggle(); // true
      if (textController.text.isEmpty) {
        throw "Keyword text required !";
      }

      const baseUrl = 'https://api.stability.ai';
      final url = Uri.parse(
          '$baseUrl/v1alpha/generation/stable-diffusion-512-v2-0/text-to-image');

      // Make the HTTP POST request to the Stability Platform API
      final response = await http.post(
        url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': "Bearer ${ConstantsClass.stabilityAPIKey}",
          'Accept': 'image/png',
        },
        body: jsonEncode({
          'cfg_scale': 7,
          'clip_guidance_preset': 'FAST_BLUE',
          'height': 1024,
          'width': 1024,
          'samples': 1,
          'steps': 50,
          'text_prompts': [
            {
              'text': textController.text,
              'weight': 1,
            }
          ],
        }),
      );
      isLoading.toggle(); // false
      if (response.statusCode != 200) {
        Get.snackbar("ERROR",
            "${response.statusCode.toString()} Failed to generate image");
      } else {
        try {
          imageData = (response.bodyBytes);
          imageListInt = extractUint8List(response.bodyBytes);

          // print(imageData);
          // print("================================================");
          // print(imageListInt);
        } on Exception catch (e) {
          Get.snackbar("ERROR", "Failed to generate image: ${e.toString()}");
        }
      }
    } catch (e) {
      isLoading.toggle();
      Get.snackbar("Error", e.toString());
    }
  }

  Future<bool> requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  void downloadImg() async {
    try {
      loadingDownload.toggle();
      Uint8List responseImg = await http
          .get(Uri.parse(imageOutput.value))
          .then((value) => value.bodyBytes);
      final dir = await getApplicationDocumentsDirectory();
      var status = await Permission.storage.status;
      if (!status.isGranted) {
        await Permission.storage.request();
      }
      File imageFile = File(path.join(dir.path,
          "aitrust_nftimage_${DateTime.now().millisecondsSinceEpoch}.png"));
      if (!await imageFile.exists()) {
        imageFile.create(recursive: true);
      }
      await imageFile.writeAsBytes(responseImg);

      if (Platform.isAndroid) {
        final externalURL = await getExternalStorageDirectory();
        final pathDonwload = externalURL!.path.split("/Android")[0];
        await imageFile.copy(
            "$pathDonwload/Download/aitrust_nftimage_${DateTime.now().millisecondsSinceEpoch}.png");
      }

      Get.snackbar("Downloaded", "Image has been downloaded");
      loadingDownload.toggle();
    } catch (e) {
      loadingDownload.toggle();
      Get.snackbar("ERROR", e.toString());
    }
  }

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  // @override
  // void onClose() {
  //   super.onClose();
  // }
}

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:get/get.dart';

import '../controllers/createnft_controller.dart';

class CreatenftView extends GetView<CreatenftController> {
  const CreatenftView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create NFT by AI'),
        centerTitle: false,
        elevation: 0,
      ),
      body: ListView(
        children: [
          Obx(
            () => (!controller.isLoading.value &&
                    controller.imageOutput.value != "")
                ? SizedBox(
                    width: double.infinity,
                    child: Column(
                      children: [
                        Image.network(
                          controller.imageOutput.value,
                          loadingBuilder: (context, child, loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: LinearProgressIndicator(
                                value: loadingProgress.expectedTotalBytes !=
                                        null
                                    ? loadingProgress.cumulativeBytesLoaded /
                                        loadingProgress.expectedTotalBytes!
                                    : null,
                                color: Colors.amber,
                              ),
                            );
                          },
                          fit: BoxFit.contain,
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          width: 150,
                          child: Obx(
                            () => controller.loadingDownload.value
                                ? const SpinKitThreeBounce(
                                    color: Colors.blue,
                                    size: 16,
                                  )
                                : ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      elevation: 0,
                                      shape: const StadiumBorder(),
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 13.0,
                                      ),
                                      backgroundColor: Colors.blue,
                                    ),
                                    onPressed: () => controller.downloadImg(),
                                    child: const Text("Download Image"),
                                  ),
                          ),
                        )
                      ],
                    ),
                  )
                : const Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: Center(child: Text("")),
                  ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                TextField(
                  autofocus: true,
                  autocorrect: false,
                  controller: controller.textController,
                  decoration: const InputDecoration(
                    labelText: "Keyword",
                    labelStyle: TextStyle(
                      color: Colors.grey,
                    ),
                    border: InputBorder.none,
                    hintText: 'Enter your input keyword',
                    filled: true,
                    // contentPadding: const EdgeInsets.all(16),
                    // labelStyle: TextStyle(color: Colors.red),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Obx(
                  () => controller.isLoading.value
                      ? const SpinKitThreeBounce(
                          color: Colors.blue,
                          size: 16,
                        )
                      : SizedBox(
                          width: 150,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              elevation: 0,
                              shape: const StadiumBorder(),
                              padding: const EdgeInsets.symmetric(
                                vertical: 13.0,
                              ),
                              backgroundColor: Colors.blue,
                            ),
                            onPressed: controller.generateImg,
                            child: const Text('Generate Image'),
                          ),
                        ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

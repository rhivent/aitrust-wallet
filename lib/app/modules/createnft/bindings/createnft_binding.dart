import 'package:get/get.dart';

import '../controllers/createnft_controller.dart';

class CreatenftBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CreatenftController>(
      () => CreatenftController(),
    );
  }
}

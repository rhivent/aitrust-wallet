import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ImportWalletController extends GetxController {
  final loadingDt = false.obs;
  final scanQrResult = "".obs;

  //d581816bdfb7115c88ff19c7da6057561697e69a8b5f1cffba152fe2c0e66814
  TextEditingController seedC = TextEditingController(text: "");

  @override
  void onClose() {
    super.onClose();
    seedC.dispose();
  }
}

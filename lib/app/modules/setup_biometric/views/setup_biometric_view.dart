import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/setup_biometric_controller.dart';

class SetupBiometricView extends GetView<SetupBiometricController> {
  const SetupBiometricView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SetupBiometricView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'SetupBiometricView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}

import 'package:get/get.dart';

import '../controllers/setup_biometric_controller.dart';

class SetupBiometricBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SetupBiometricController>(
      () => SetupBiometricController(),
    );
  }
}

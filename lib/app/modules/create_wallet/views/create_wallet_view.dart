import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/create_wallet_controller.dart';

class CreateWalletView extends GetView<CreateWalletController> {
  final walletC = Get.find<WalletController>();

  CreateWalletView({super.key});

  @override
  Widget build(BuildContext context) {
    late List<String> dtlist = walletC.datawal['seed']?.split(" ") ?? [];

    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Wallet'),
        centerTitle: true,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Center(
              child: Text(
                'Secret Recovery Phrase',
                style: TextStyle(
                  fontSize: 24,
                  height: 1.4,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const SizedBox(
              width: 310,
              child: Center(
                child: Text(
                  'Generate secret phrases',
                  style: TextStyle(
                    fontSize: 14,
                    height: 1.4,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const SizedBox(
              height: 36,
            ),
            Obx(
              () => GridView.count(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                crossAxisCount: 2,
                childAspectRatio: 4,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                children: [
                  ...dtlist.asMap().entries.map((
                    dtx,
                  ) {
                    return Container(
                      padding: const EdgeInsets.only(left: 25),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: const Color.fromRGBO(250, 250, 250, 0.04),
                          style: BorderStyle.solid,
                          width: 1.0,
                        ),
                        color: const Color.fromRGBO(28, 29, 33, 1),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      width: 165,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "${((dtx.key + 1).toString())}.",
                            style: const TextStyle(color: Colors.white),
                          ),
                          const VerticalDivider(
                            color: Color.fromRGBO(255, 255, 255, 0.043),
                            thickness: 1,
                          ),
                          Text(
                            dtx.value,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    );
                  }),
                ],
              ),
            ),
            const SizedBox(
              height: 36,
            ),
            TextButton(
              onPressed: () async {
                await Clipboard.setData(
                    ClipboardData(text: walletC.datawal['seed']));
                Get.snackbar("Success", "Success copied !");
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    "Copy to clipboard",
                    style: TextStyle(
                      height: 1.4,
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Icon(Icons.copy),
                ],
              ),
            ),
            SizedBox(
              width: 150,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  shape: const StadiumBorder(),
                  padding: const EdgeInsets.symmetric(
                    vertical: 10.0,
                  ),
                ),
                onPressed: () => Get.toNamed(Routes.SETUP_PASSWORD),
                child: const Text(
                  "Next",
                  style: TextStyle(
                    height: 1.4,
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

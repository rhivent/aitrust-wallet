import 'package:get/get.dart';
import '../models/models_model.dart';
import '../services/api_service.dart';

import '../models/chat_model.dart';
import 'package:flutter/material.dart';

class ChatgptController extends GetxController {
  final isTyping = false.obs;
  final isFirstLoading = true.obs;
  late TextEditingController textEditingController =
      TextEditingController(text: "");
  late ScrollController listScrollController = ScrollController();
  late FocusNode focusNode = FocusNode();

  List<ChatModel> chatList = <ChatModel>[].obs;
  List<ModelsModel> modelsList = [];
  String currentModel = "text-davinci-003";

  List<ChatModel> get getChatList {
    return chatList;
  }

  void addUserMessage({required String msg}) {
    chatList.add(ChatModel(msg: msg, chatIndex: 0));
  }

  Future<void> sendMessageAndGetAnswers(
      {required String msg, required String chosenModelId}) async {
    chatList.addAll(await ApiService.sendMessage(
      message: msg,
      modelId: chosenModelId,
    ));
  }

  String get getCurrentModel {
    return currentModel;
  }

  void setCurrentModel(String newModel) {
    currentModel = newModel;
  }

  List<ModelsModel> get getModelsList {
    return modelsList;
  }

  Future<List<ModelsModel>> getAllModels() async {
    var data = await ApiService.getModels();
    modelsList = data;
    return modelsList;
  }

  @override
  void onInit() {
    listScrollController = ScrollController();
    textEditingController = TextEditingController();
    focusNode = FocusNode();
    super.onInit();
  }

  @override
  void onClose() {
    listScrollController.dispose();
    textEditingController.dispose();
    focusNode.dispose();
    super.onClose();
  }
}

import '../controllers/chatgpt_controller.dart';

import '../widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../constants/constants.dart';
import '../models/models_model.dart';

class ModelsDrowDownWidget extends StatelessWidget {
  final homeC = Get.find<ChatgptController>();

  ModelsDrowDownWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ModelsModel>>(
        future: homeC.getAllModels(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting &&
              homeC.isFirstLoading.value == true) {
            homeC.isFirstLoading.value = false;
            return const FittedBox(
              child: SpinKitFadingCircle(
                color: Colors.lightBlue,
                size: 30,
              ),
            );
          }
          if (snapshot.hasError) {
            return Center(
              child: TextWidget(
                label: snapshot.error.toString(),
                color: Colors.white,
              ),
            );
          }
          return snapshot.data == null || snapshot.data!.isEmpty
              ? const SizedBox.shrink()
              : FittedBox(
                  child: DropdownButton(
                    dropdownColor: scaffoldBackgroundColor,
                    iconEnabledColor: Colors.white,
                    items: List<DropdownMenuItem<String>>.generate(
                        snapshot.data!.length,
                        (index) => DropdownMenuItem(
                            value: snapshot.data![index].id,
                            child: TextWidget(
                              color: Colors.white,
                              label: snapshot.data![index].id,
                              fontSize: 15,
                            ))),
                    value: homeC.currentModel,
                    onChanged: (value) {
                      homeC.setCurrentModel(
                        value.toString(),
                      );
                    },
                  ),
                );
        });
  }
}

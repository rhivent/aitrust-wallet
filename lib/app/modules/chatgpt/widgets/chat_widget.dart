import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:walletdapps/app/modules/chatgpt/controllers/chatgpt_controller.dart';
import '../services/assets_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'text_widget.dart';

class ChatWidget extends StatelessWidget {
  final chatGptC = Get.find<ChatgptController>();
  ChatWidget({
    super.key,
    required this.indexdt,
  });

  // final String msg;
  final int indexdt;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Material(
          color: Colors.grey[300],
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  chatGptC.getChatList[indexdt].chatIndex == 0
                      ? AssetsManager.userImage
                      : AssetsManager.botImage,
                  height: 30,
                  width: 30,
                ),
                const SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: chatGptC.getChatList[indexdt].chatIndex == 0
                      ? TextWidget(
                          label: chatGptC.getChatList[indexdt].msg,
                        )
                      : DefaultTextStyle(
                          style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 16),
                          child: AnimatedTextKit(
                              isRepeatingAnimation: false,
                              repeatForever: false,
                              displayFullTextOnTap: true,
                              totalRepeatCount: 1,
                              animatedTexts: [
                                TyperAnimatedText(
                                    chatGptC.getChatList[indexdt].msg.trim(),
                                    speed: const Duration(milliseconds: 10)),
                              ]),
                        ),
                ),
                // chatIndex == 0
                //     ? const SizedBox.shrink()
                //     : Row(
                //         mainAxisAlignment: MainAxisAlignment.end,
                //         mainAxisSize: MainAxisSize.min,
                //         children: const [
                //           Icon(Icons.thumb_up_alt_outlined),
                //           SizedBox(
                //             width: 5,
                //           ),
                //           Icon(Icons.thumb_down_alt_outlined)
                //         ],
                //       ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

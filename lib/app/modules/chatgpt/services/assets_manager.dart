class AssetsManager {
  static String imagePath = "assets/img";
  static String userImage = "$imagePath/person.png";
  static String botImage = "$imagePath/robotchat.png";
  static String openaiLogo = "$imagePath/chat.png";
}

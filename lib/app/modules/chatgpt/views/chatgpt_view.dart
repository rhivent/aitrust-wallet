import 'dart:developer';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:walletdapps/widgets/bottomnav_widget.dart';

import '../controllers/chatgpt_controller.dart';

import '../services/services.dart';

import '../services/assets_manager.dart';

import '../widgets/chat_widget.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChatgptView extends GetView<ChatgptController> {
  const ChatgptView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        // leading: Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child: Image.asset(AssetsManager.openaiLogo),
        // ),
        title: const Text("Chat GPT"),
        actions: [
          IconButton(
            onPressed: () async {
              await Services.showModalSheet(context: context);
            },
            icon: const Icon(Icons.more_vert_rounded),
          ),
        ],
        centerTitle: false,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Flexible(
              child: Obx(
                () => ListView.builder(
                    controller: controller.listScrollController,
                    itemCount: controller.getChatList.length, //chatList.length,
                    itemBuilder: (context, index) {
                      return ChatWidget(
                        indexdt: index,
                        // msg: controller
                        //     .getChatList[index].msg, // chatList[index].msg,
                        // chatIndex: controller.getChatList[index]
                        //     .chatIndex, //chatList[index].chatIndex,
                      );
                    }),
              ),
            ),
            Obx(
              () => controller.isTyping.value
                  ? const SpinKitThreeBounce(
                      color: Colors.blue,
                      size: 14,
                    )
                  : const SizedBox(),
            ),
            const SizedBox(
              height: 5,
            ),
            Material(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        autocorrect: false,
                        autofocus: true,
                        focusNode: controller.focusNode,
                        controller: controller.textEditingController,
                        onSubmitted: (value) async {
                          await sendMessageFCT();
                        },
                        decoration: const InputDecoration.collapsed(
                            hintText: "How can I help you",
                            hintStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    IconButton(
                      onPressed: () async {
                        await sendMessageFCT();
                      },
                      icon: const Icon(Icons.send),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavComp(),
    );
  }

  void scrollListToEND() {
    controller.listScrollController.animateTo(
        controller.listScrollController.position.maxScrollExtent,
        duration: const Duration(seconds: 2),
        curve: Curves.easeOut);
  }

  Future<void> sendMessageFCT() async {
    if (controller.isTyping.value) {
      Get.snackbar("Error", "You cant send multiple messages at a time");
      return;
    }
    if (controller.textEditingController.text.isEmpty) {
      Get.snackbar("Error", "Please type a message");
      return;
    }
    try {
      String msg = controller.textEditingController.text;
      controller.isTyping.toggle();
      controller.addUserMessage(msg: msg);
      controller.textEditingController.clear();
      controller.focusNode.unfocus();
      await controller.sendMessageAndGetAnswers(
          msg: msg, chosenModelId: controller.getCurrentModel);
      // chatList.addAll(await ApiService.sendMessage(
      //   message: textEditingController.text,
      //   modelId: modelsProvider.getCurrentModel,
      // ));
    } catch (error) {
      log("error $error");
      Get.snackbar("Error", error.toString());
    } finally {
      scrollListToEND();
      controller.isTyping.toggle();
    }
  }
}

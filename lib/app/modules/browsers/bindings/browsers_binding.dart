import 'package:get/get.dart';

import '../controllers/browsers_controller.dart';

class BrowsersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BrowsersController>(
      () => BrowsersController(),
    );
  }
}

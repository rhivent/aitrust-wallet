import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/widgets/bottomnav_widget.dart';

import '../controllers/browsers_controller.dart';

class BrowsersView extends GetView<BrowsersController> {
  final GlobalKey webViewKey = GlobalKey();
  BrowsersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final urlAwal = Get.arguments == null
        ? "https://aitrust-web3.vercel.app/"
        : Get.arguments as String;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Browser'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () => controller.webViewController?.goBack(),
                    child: const Icon(Icons.arrow_back_ios),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  GestureDetector(
                    onTap: () => controller.webViewController?.goForward(),
                    child: const Icon(Icons.arrow_forward_ios),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  GestureDetector(
                    onTap: () => controller.webViewController?.reload(),
                    child: const Icon(Icons.refresh),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: TextField(
                      autocorrect: false,
                      // autofocus: true,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                      ),
                      controller: controller.urlController,
                      keyboardType: TextInputType.url,
                      textInputAction: TextInputAction.go,
                      onSubmitted: (value) {
                        var url = Uri.parse(value);
                        if (url.scheme.isEmpty) {
                          url = Uri.parse(
                              "https://www.google.com/search?q=$value");
                        }
                        controller.webViewController
                            ?.loadUrl(urlRequest: URLRequest(url: url));
                      },
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  InAppWebView(
                    key: webViewKey,
                    initialUrlRequest: URLRequest(url: Uri.parse(urlAwal)),
                    initialOptions: controller.options,
                    pullToRefreshController: controller.pullToRefreshController,
                    onWebViewCreated: (ctrl) {
                      controller.webViewController = ctrl;
                    },
                    onLoadStart: (ctrl, url) {
                      controller.url.value = url.toString();
                      controller.urlController.text = url.toString();
                    },
                    androidOnPermissionRequest:
                        (ctrl, origin, resources) async {
                      return PermissionRequestResponse(
                          resources: resources,
                          action: PermissionRequestResponseAction.GRANT);
                    },
                    shouldOverrideUrlLoading: (ctrl, navigationAction) async {
                      var uri = navigationAction.request.url!;
                      if (![
                        "http",
                        "https",
                        "file",
                        "chrome",
                        "data",
                        "javascript",
                        "about"
                      ].contains(uri.scheme)) {
                        if (uri.toString().contains("/apps/")) {
                          if (uri.toString().contains("apps/receive")) {
                            Get.toNamed(Routes.RECEIVETX);
                          } else if (uri
                              .toString()
                              .contains("apps/sendtoken")) {
                            final addressToken = uri
                                .toString()
                                .split("sendtoken?addressToken=")[1];
                            Get.toNamed(Routes.SENDTX, arguments: {
                              "token_address": addressToken,
                              "name": "Token",
                              "symbol": "Token",
                              "logo": "",
                              "thumbnail": "",
                              "decimals": 18,
                              "balance": 0,
                            });
                          } else {
                            Get.toNamed(Routes.HOME);
                          }
                          return NavigationActionPolicy.CANCEL;
                        }
                        if (await canLaunchUrl(
                            Uri.parse(controller.url.value))) {
                          // Launch the App
                          await launchUrl(
                            Uri.parse(controller.url.value),
                            mode: LaunchMode.externalNonBrowserApplication,
                          );
                          // and cancel the request
                          return NavigationActionPolicy.CANCEL;
                        }
                      }

                      return NavigationActionPolicy.ALLOW;
                    },
                    onLoadStop: (ctrl, url) async {
                      controller.pullToRefreshController.endRefreshing();
                      controller.url.value = url.toString();
                      controller.urlController.text = url.toString();
                    },
                    onLoadError: (ctrl, url, code, message) {
                      controller.pullToRefreshController.endRefreshing();
                    },
                    onProgressChanged: (ctrl, progress) {
                      if (progress == 100) {
                        controller.pullToRefreshController.endRefreshing();
                      }
                      controller.progress.value = progress / 100;
                      controller.urlController.text =
                          controller.url.value.toString();
                    },
                    onUpdateVisitedHistory: (ctrl, url, androidIsReload) {
                      controller.url.value = url.toString();
                      controller.urlController.text = url.toString();
                    },
                    onConsoleMessage: (controller, consoleMessage) {
                      // print(consoleMessage);
                    },
                  ),
                  Obx(
                    () => controller.progress.value < 1.0
                        ? LinearProgressIndicator(
                            value: controller.progress.value)
                        : Container(),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavComp(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/sendnft_controller.dart';

class SendnftView extends GetView<SendnftController> {
  SendnftView({Key? key}) : super(key: key);
  final walletC = Get.find<WalletController>();

  @override
  Widget build(BuildContext context) {
    var arguments =
        Get.arguments == null ? {} : Get.arguments as Map<String, dynamic>;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Send NFT"),
        centerTitle: true,
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 10,
        ),
        children: [
          const SizedBox(
            height: 32,
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Recepient Address",
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.zero,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: TextField(
                controller: controller.recepientC,
                autofocus: true,
                autocorrect: false,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.qr_code_scanner),
                    onPressed: () {
                      scanQR();
                    },
                  ),
                  hintText: "Enter recepient address here",
                ),
              ),
            ),
          ),
          const Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Amount",
              ),
            ),
          ),
          arguments['contract_type'] == "ERC1155"
              ? Card(
                  margin: EdgeInsets.zero,
                  elevation: 0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 13),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      controller: controller.amountC,
                      autocorrect: false,
                      decoration: const InputDecoration.collapsed(
                        hintText: "Input amount of nft 1",
                      ),
                    ),
                  ),
                )
              : Container(),
          Obx(
            () => controller.loadingSend.value
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      shape: const StadiumBorder(),
                      padding: const EdgeInsets.symmetric(
                        vertical: 13.0,
                      ),
                      backgroundColor: Colors.blue,
                    ),
                    onPressed: () async {
                      try {
                        controller.loadingSend.toggle();
                        if (controller.recepientC.text.isEmpty) {
                          throw Exception("Recepient must be filled !");
                        }
                        final txhash = await walletC.sendNFT(
                          controller.recepientC.text,
                          controller.amountC.text,
                          arguments,
                        );
                        final tx =
                            "${walletC.selectedBlockchain.explorerUrl}tx/$txhash";
                        Clipboard.setData(ClipboardData(text: tx));
                        Get.snackbar(
                          "Success",
                          "Tx Hash: $tx has been COPIED !",
                          duration: const Duration(seconds: 8),
                        );
                        controller.loadingSend.toggle();
                        Get.offAllNamed(Routes.HOME);
                      } catch (e) {
                        Get.snackbar(
                          "Error",
                          e.toString(),
                        );
                        controller.loadingSend.toggle();
                      }
                    },
                    child: const Text("Send"),
                  ),
          ),
        ],
      ),
    );
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666',
        'Cancel',
        true,
        ScanMode.QR,
      );
      controller.scanQrResult.value = barcodeScanRes;
      controller.recepientC.text = barcodeScanRes;
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SendnftController extends GetxController {
  TextEditingController recepientC = TextEditingController(text: "");
  TextEditingController amountC = TextEditingController(text: "1");
  final loadingSend = false.obs;
  final scanQrResult = "".obs;

  @override
  void onClose() {
    recepientC.dispose();
    amountC.dispose();
    super.onClose();
  }
}

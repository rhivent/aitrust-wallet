import 'package:get/get.dart';

import '../controllers/sendnft_controller.dart';

class SendnftBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SendnftController>(
      () => SendnftController(),
    );
  }
}

import 'dart:math';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/global_controller.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/data/listnetworkblockchain_model.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/config/constantsData.dart';
import 'package:walletdapps/widgets/bottomnav_widget.dart';
import 'package:walletdapps/widgets/drawer_widget.dart';
import '../controllers/home_controller.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeView extends GetView<HomeController> {
  final globalC = Get.find<GlobalController>();
  final walletC = Get.find<WalletController>();

  HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: SizedBox(
          height: 40,
          child: Image.asset(
            Assetmanager.logoHz,
            fit: BoxFit.contain,
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () => walletC.checkBalance(),
            icon: const Icon(Icons.refresh),
          ),
        ],
      ),
      body: SafeArea(
        child: Obx(
          () => globalC.loadingGlobal
              ? const Center(child: CircularProgressIndicator())
              : ListView(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  children: [
                    Container(
                      width: 330,
                      padding: const EdgeInsets.only(
                          left: 24, right: 24, top: 21, bottom: 21),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        gradient: const LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            Color.fromARGB(255, 84, 190, 255),
                            Color(0xFF0002D4)
                          ],
                          transform: GradientRotation(0.62889),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: 200,
                            padding: EdgeInsets.zero,
                            decoration: BoxDecoration(
                              color: const Color.fromRGBO(217, 217, 217, 0.31),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            child: DropdownSearch<Listnetworkblockchain>(
                              popupProps: PopupProps.menu(
                                containerBuilder: (context, popupWidget) {
                                  return Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: popupWidget,
                                  );
                                },
                                showSearchBox: true,
                                searchFieldProps: TextFieldProps(
                                  controller: controller.searchDropdownC,
                                  decoration: const InputDecoration(),
                                ),
                                itemBuilder: (_, item, isSelected) {
                                  var constClass = BlockchainData.fromJsonList(
                                      ConstantsClass.networkData);
                                  final image = constClass.firstWhere(
                                      (element) =>
                                          element.chainId ==
                                          (int.tryParse(item.value!) ?? 97));
                                  return Container(
                                    padding: EdgeInsets.zero,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 2),
                                    decoration: !isSelected
                                        ? null
                                        : BoxDecoration(
                                            border: Border.all(
                                                color: Theme.of(context)
                                                    .primaryColor),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: Colors.white,
                                          ),
                                    child: ListTile(
                                      selected: isSelected,
                                      title: Text(item.name.toString()),
                                      leading: ClipOval(
                                        child: Image.network(
                                          image.logo,
                                          width: 30,
                                          height: 30,
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                              selectedItem: Listnetworkblockchain.fromJson(
                                  walletC.selectedNetwork),
                              dropdownDecoratorProps:
                                  const DropDownDecoratorProps(
                                dropdownSearchDecoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Cari Network",
                                  fillColor: Colors.amber,
                                  constraints: BoxConstraints(minWidth: 120),
                                  contentPadding: EdgeInsets.all(8),
                                ),
                                baseStyle: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                                textAlignVertical: TextAlignVertical.center,
                              ),
                              itemAsString: (Listnetworkblockchain? u) =>
                                  u!.name.toString(),
                              items: Listnetworkblockchain.fromJsonList(
                                  ConstantsClass.listStringNetwork),
                              onChanged: (val) {
                                if (val != null) {
                                  final constClass =
                                      BlockchainData.fromJsonList(
                                          ConstantsClass.networkData);
                                  final blockChainSelect =
                                      constClass.firstWhere((element) =>
                                          element.chainId ==
                                          (int.tryParse(val.value!) ?? 97));
                                  // print(blockChainSelect);
                                  walletC.selectedBlockchain = blockChainSelect;
                                  walletC.changeBlockchain(
                                      (int.tryParse(val.value!) ?? 97));
                                } else {
                                  // print("nothing");
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                width: 30,
                                height: 30,
                                padding: const EdgeInsets.all(2),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.white,
                                ),
                                child: Image.network(
                                  walletC.selectedBlockchain.logo != ""
                                      ? walletC.selectedBlockchain.logo
                                      : "",
                                  fit: BoxFit.contain,
                                ),
                              ),
                              Text(
                                walletC.selectedBlockchain.nativeCoin,
                                style: const TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: Obx(
                              () => Text(
                                walletC.balance.length < 11
                                    ? walletC.balance
                                    : walletC.balance.substring(0, 10),
                                style: const TextStyle(
                                  fontSize: 36,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            child: TextButton.icon(
                              onPressed: () async {
                                await Clipboard.setData(ClipboardData(
                                    text: walletC.datawal['pub']));
                                Get.snackbar(
                                    "Success", "Success copied address");
                              },
                              icon: const Icon(
                                Icons.circle,
                                size: 10,
                                color: Color(0xFF66E4E7),
                              ),
                              label: Text(
                                "${walletC.datawal['pub'] != "" ? walletC.datawal['pub']!.substring(0, 10) : ''}...",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF66E4E7),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   width: double.infinity,
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: [
                    //       Column(
                    //         children: [
                    //           SizedBox.fromSize(
                    //             size: const Size(56, 56),
                    //             child: ClipOval(
                    //               child: Material(
                    //                 color: const Color.fromRGBO(
                    //                     255, 255, 255, 0.02),
                    //                 child: InkWell(
                    //                   splashColor: Colors.blueGrey,
                    //                   onTap: () => Get.toNamed(Routes.SENDTX),
                    //                   child: Column(
                    //                     mainAxisAlignment:
                    //                         MainAxisAlignment.center,
                    //                     children: const [
                    //                       Icon(
                    //                         Icons.arrow_upward_rounded,
                    //                       ), // <-- Icon
                    //                     ],
                    //                   ),
                    //                 ),
                    //               ),
                    //             ),
                    //           ),
                    //           const SizedBox(
                    //             height: 8,
                    //           ),
                    //           const Text(
                    //             'Send',
                    //             style: TextStyle(
                    //               fontSize: 14.0,
                    //               fontWeight: FontWeight.w700,
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //       const SizedBox(
                    //         width: 40,
                    //       ),
                    //       Column(
                    //         children: [
                    //           SizedBox.fromSize(
                    //             size: const Size(56, 56),
                    //             child: ClipOval(
                    //               child: Material(
                    //                 color: const Color.fromRGBO(
                    //                     255, 255, 255, 0.02),
                    //                 child: InkWell(
                    //                   splashColor: Colors.blueGrey,
                    //                   onTap: () =>
                    //                       Get.toNamed(Routes.RECEIVETX),
                    //                   child: Column(
                    //                     mainAxisAlignment:
                    //                         MainAxisAlignment.center,
                    //                     children: const [
                    //                       Icon(Icons
                    //                           .arrow_downward_rounded), // <-- Icon
                    //                     ],
                    //                   ),
                    //                 ),
                    //               ),
                    //             ),
                    //           ),
                    //           const SizedBox(
                    //             height: 8,
                    //           ),
                    //           const Text(
                    //             'Receive',
                    //             style: TextStyle(
                    //               fontSize: 14.0,
                    //               fontWeight: FontWeight.w700,
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //       const SizedBox(
                    //         width: 40,
                    //       ),
                    //       Column(
                    //         children: [
                    //           SizedBox.fromSize(
                    //             size: const Size(56, 56),
                    //             child: ClipOval(
                    //               child: Material(
                    //                 color: const Color.fromRGBO(
                    //                     255, 255, 255, 0.02),
                    //                 child: InkWell(
                    //                   splashColor: Colors.blueGrey,
                    //                   onTap: () {},
                    //                   child: Column(
                    //                     mainAxisAlignment:
                    //                         MainAxisAlignment.center,
                    //                     children: const [
                    //                       Icon(Icons
                    //                           .card_giftcard_rounded), // <-- Icon
                    //                     ],
                    //                   ),
                    //                 ),
                    //               ),
                    //             ),
                    //           ),
                    //           const SizedBox(
                    //             height: 8,
                    //           ),
                    //           const Text(
                    //             'Buy',
                    //             style: TextStyle(
                    //               fontSize: 14.0,
                    //               fontWeight: FontWeight.w700,
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //       const SizedBox(
                    //         width: 40,
                    //       ),
                    //       Column(
                    //         children: [
                    //           SizedBox.fromSize(
                    //             size: const Size(56, 56),
                    //             child: ClipOval(
                    //               child: Material(
                    //                 color: const Color.fromRGBO(
                    //                     255, 255, 255, 0.02),
                    //                 child: InkWell(
                    //                   splashColor: Colors.blueGrey,
                    //                   onTap: () => walletC.checkUnit(),
                    //                   child: Column(
                    //                     mainAxisAlignment:
                    //                         MainAxisAlignment.center,
                    //                     children: const [
                    //                       Icon(Icons
                    //                           .swap_vert_rounded), // <-- Icon
                    //                     ],
                    //                   ),
                    //                 ),
                    //               ),
                    //             ),
                    //           ),
                    //           const SizedBox(
                    //             height: 8,
                    //           ),
                    //           const Text(
                    //             'Swap',
                    //             style: TextStyle(
                    //               fontSize: 14.0,
                    //               fontWeight: FontWeight.w700,
                    //             ),
                    //           )
                    //         ],
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    const SizedBox(
                      height: 16,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 11),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ...menuHome
                              .asMap()
                              .entries
                              .map(
                                (e) => GestureDetector(
                                  onTap: () {
                                    try {
                                      switch (e.value["label"]) {
                                        case "SEND":
                                          Get.toNamed(Routes.SENDTX);
                                          break;
                                        case "RECEIVE":
                                          Get.toNamed(Routes.RECEIVETX);
                                          break;
                                        case "SWAP":
                                          // Get.toNamed(
                                          //   Routes.BROWSERS,
                                          //   arguments:
                                          //       "https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A",
                                          // );
                                          if (walletC.selectedChainId.value ==
                                                  56 ||
                                              walletC.selectedChainId.value ==
                                                  97) {
                                            Get.toNamed(Routes.SWAP);
                                          } else {
                                            throw "Only support network for Binance Smart Chain(BSC) mainnet and testnet";
                                          }
                                          break;
                                        case "BUY":
                                          Get.toNamed(
                                            Routes.BROWSERS,
                                            arguments:
                                                "https://widget.onramper.com?color=1CA7EC&defaultCrypto=BNB&apiKey=pk_prod_1yymCXaDroiCHCWFy8PYGYelkmJSQF2xjv3fvq2hejY0",
                                          );
                                          break;
                                        default:
                                          walletC.checkUnit();
                                          break;
                                      }
                                    } catch (e) {
                                      Get.snackbar(
                                          "Error Router", e.toString());
                                    }
                                  },
                                  child: Container(
                                    height: 28,
                                    width: 67,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8.0),
                                      color: const Color(0xFF080701),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          e.value["icon"] as IconData,
                                          color: Colors.white,
                                          size: 13,
                                        ),
                                        const SizedBox(
                                          width: 4,
                                        ),
                                        Text(
                                          "${e.value['label']}",
                                          style: const TextStyle(
                                            fontSize: 10,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton(
                          onPressed: () async {
                            controller.activeTypeToken.value = "erc20";
                            controller.loadingToken.toggle();
                            await walletC.getAllTokens();
                            controller.loadingToken.toggle();
                          },
                          child: Obx(
                            () => Text(
                              'Tokens',
                              style: TextStyle(
                                color:
                                    controller.activeTypeToken.value == "erc20"
                                        ? Colors.blue
                                        : Colors.grey,
                              ),
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () async {
                            controller.activeTypeToken.value = "nfts";
                            controller.loadingToken.toggle();
                            await walletC.getAllNFTs();
                            controller.loadingToken.toggle();
                          },
                          child: Obx(
                            () => Text(
                              'NFTs',
                              style: TextStyle(
                                color:
                                    controller.activeTypeToken.value == "nfts"
                                        ? Colors.blue
                                        : Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Obx(
                      () => controller.loadingToken.value
                          ? const Center(
                              child: CircularProgressIndicator(),
                            )
                          : tokenRender(controller.activeTypeToken.value),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
        ),
      ),
      drawer: DrawerComponent(),
      bottomNavigationBar: BottomNavComp(),
    );
  }

  final menuHome = [
    {
      "icon": Icons.send,
      "label": "SEND",
    },
    {
      "icon": Icons.download,
      "label": "RECEIVE",
    },
    {
      "icon": Icons.price_check,
      "label": "BUY",
    },
    {
      "icon": Icons.mobiledata_off,
      "label": "SWAP",
    },
  ];

  Widget tokenRender(String typeData) {
    switch (typeData) {
      case "nfts":
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextButton.icon(
              onPressed: () {
                Get.toNamed(Routes.CREATENFT);
              },
              icon: const Icon(Icons.control_point_duplicate_sharp),
              label: const Text("Create Image NFT"),
            ),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              padding: const EdgeInsets.all(10),
              itemBuilder: (context, index) {
                if (walletC.nftsByAddress.isEmpty) {
                  return const Center(child: Text("No Data"));
                }
                return GestureDetector(
                  onTap: () => Get.toNamed(
                    Routes.DETAIL_NFT,
                    arguments: walletC.nftsByAddress[index],
                  ),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          walletC.nftsByAddress[index]["normalized_metadata"]
                                      ["image"] !=
                                  null
                              ? Image.network(walletC.nftsByAddress[index]
                                      ["normalized_metadata"]["image"]
                                  .replaceAll(
                                      "ipfs.infura.io", "nftstorage.link")
                                  .replaceAll("ipfs://",
                                      "http://nftstorage.link/ipfs/"))
                              : const Center(
                                  child: Text("No Image"),
                                ),
                          const SizedBox(
                            height: 12,
                          ),
                          GestureDetector(
                            onTap: () {
                              Clipboard.setData(
                                ClipboardData(
                                  text: walletC.nftsByAddress[index]
                                      ['token_address'],
                                ),
                              );
                              Get.snackbar("Success",
                                  "Success Copied NFT address ${walletC.nftsByAddress[index]["normalized_metadata"]["name"]} Address !");
                            },
                            child: Text(
                              '${walletC.nftsByAddress[index]["normalized_metadata"]["name"] ?? walletC.nftsByAddress[index]["name"]}',
                              style: const TextStyle(
                                color: Colors.blue,
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Text(
                            '${walletC.nftsByAddress[index]["contract_type"]} (amount: ${walletC.nftsByAddress[index]["amount"]})',
                            style: const TextStyle(
                              fontSize: 13,
                              color: Colors.grey,
                            ),
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Text(
                            '${walletC.nftsByAddress[index]["normalized_metadata"]["description"]}',
                            style: const TextStyle(
                              fontSize: 14,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (context, idx) => const SizedBox(
                height: 16,
              ),
              itemCount: walletC.nftsByAddress.length,
            ),
          ],
        );
      default:
        return Column(
          children: [
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => {
                    Get.toNamed(
                      Routes.DETAIL_TOKEN,
                      arguments: walletC.tokensByAddress[index],
                    )
                  },
                  child: SingleChildScrollView(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            // Container(
                            //   width: 40,
                            //   height: 40,
                            //   decoration: const BoxDecoration(
                            //     color: Color(0xFFD9D9D9),
                            //     shape: BoxShape.circle,
                            //   ),
                            //   child: walletC.tokensByAddress[index]['logo'] !=
                            //           null
                            //       ? Image.network(
                            //           walletC.tokensByAddress[index]['logo'])
                            //       : Image.asset(Assetmanager.logoOnly),
                            // ),
                            // const SizedBox(
                            //   width: 9,
                            // ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Clipboard.setData(
                                      ClipboardData(
                                        text: walletC.tokensByAddress[index]
                                            ['token_address'],
                                      ),
                                    );
                                    Get.snackbar("Success",
                                        "Success Copied token ${walletC.tokensByAddress[index]['symbol']} Address !");
                                  },
                                  child: Text(
                                    '${walletC.tokensByAddress[index]["symbol"]}',
                                    style: const TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                ),
                                Text(
                                  '${walletC.tokensByAddress[index]["name"]}',
                                  style: const TextStyle(
                                    fontSize: 10,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Text(
                          balances(walletC.tokensByAddress[index]["balance"],
                              walletC.tokensByAddress[index]["decimals"]),
                          maxLines: 1,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, idx) => const SizedBox(
                height: 20,
              ),
              itemCount: walletC.tokensByAddress.length,
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: GestureDetector(
                onTap: () {
                  Get.dialog(
                    dialogAddToken(),
                  );
                  // Get.toNamed(Routes.IMPORT_TOKENS);
                },
                child: Container(
                  width: 117,
                  height: 28,
                  decoration: BoxDecoration(
                    color: const Color(0xFF080701),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.add_to_photos_outlined,
                        size: 13,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text(
                        "Add Token",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 9,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
    }
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666',
        'Cancel',
        true,
        ScanMode.QR,
      );
      controller.scanQrResult.value = barcodeScanRes;
      controller.addressInput.text = barcodeScanRes;
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
  }

  Widget dialogAddToken() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Material(
          color: Colors.transparent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              height: 200,
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              width: double.infinity,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromARGB(255, 84, 190, 255),
                    Color(0xFF0002D4)
                  ],
                  transform: GradientRotation(0.62889),
                ),
              ),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: () => Get.back(),
                      child: Container(
                        width: 18,
                        height: 18,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 2,
                            color: Colors.white,
                          ),
                        ),
                        child: const Icon(
                          Icons.close_rounded,
                          color: Colors.white,
                          size: 14,
                        ),
                      ),
                    ),
                  ),
                  const Text(
                    "Import Tokens",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Container(
                    width: 269,
                    height: 32,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(11),
                    ),
                    child: TextField(
                      textInputAction: TextInputAction.send,
                      controller: controller.addressInput,
                      autocorrect: false,
                      autofocus: true,
                      maxLines: 1, //or null
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        suffixIcon: IconButton(
                          padding: const EdgeInsets.all(0),
                          icon: const Icon(Icons.qr_code_scanner),
                          onPressed: () {
                            scanQR();
                          },
                        ),
                        contentPadding: const EdgeInsets.only(
                          left: 14,
                        ),
                        hintText: "Enter token address here",
                      ),
                      style: const TextStyle(fontSize: 15),
                    ),
                  ),
                  const SizedBox(
                    height: 17,
                  ),
                  Obx(
                    () => controller.loadingAddToken.value
                        ? const Center(
                            child: CircularProgressIndicator(
                              color: Colors.white,
                            ),
                          )
                        : SizedBox(
                            width: 69,
                            height: 22,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.black,
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 0,
                                  vertical: 2,
                                ),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              onPressed: () async {
                                try {
                                  if (controller.addressInput.text != "") {
                                    controller.loadingAddToken.toggle();
                                    await walletC.addTokenList(
                                        controller.addressInput.text);
                                    Get.back();
                                    controller.loadingAddToken.toggle();
                                    Get.dialog(const DialogCustom());
                                  } else {
                                    Get.snackbar(
                                        "Error", "Address field empty");
                                  }
                                } catch (e) {
                                  Get.snackbar("Error", e.toString());
                                  controller.loadingAddToken.toggle();
                                }
                              },
                              child: const Text(
                                "Add",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  String balances(String dt, dynamic decimals) {
    var bigdt = BigInt.from(double.parse(dt)) / BigInt.from(pow(10, decimals));
    var firstVal = bigdt.toString().split('.')[0];

    var secondVal = bigdt.toString().split('.')[1];
    if (secondVal.length > 8) {
      return "$firstVal.${secondVal.substring(0, 8)}";
    }
    return "$firstVal.$secondVal";
  }
}

class DialogCustom extends StatelessWidget {
  const DialogCustom({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Material(
          color: Colors.transparent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              height: 200,
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              width: double.infinity,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(30)),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color.fromARGB(255, 84, 190, 255),
                    Color(0xFF0002D4)
                  ],
                  transform: GradientRotation(0.62889),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.centerRight,
                    child: GestureDetector(
                      onTap: () => Get.back(),
                      child: Container(
                        width: 18,
                        height: 18,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 2,
                            color: Colors.white,
                          ),
                        ),
                        child: const Icon(
                          Icons.close_rounded,
                          color: Colors.white,
                          size: 14,
                        ),
                      ),
                    ),
                  ),
                  SvgPicture.asset(Assetmanager.successNotif),
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                    "Success",
                    style: TextStyle(
                      fontSize: 26,
                      color: Colors.white,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:walletdapps/app/controllers/wallet_controller.dart';

class HomeController extends GetxController {
  final activeTypeToken = "erc20".obs;
  final loadingToken = false.obs;

  late TextEditingController addressInput;
  final loadingAddToken = false.obs;
  final scanQrResult = "".obs;
  TextEditingController searchDropdownC = TextEditingController(text: "");

  @override
  void onInit() async {
    super.onInit();
    // 0x0ff81f18bcb9519ac6027c732d196945ca4d2a9a // DIGIS BSC Mainnet
    // example : 0x84b9B910527Ad5C03A9Ca831909E21e236EA7b06 // LINK BSC Testnet
    addressInput = TextEditingController(text: "");
  }

  @override
  void onClose() {
    super.onClose();
    searchDropdownC.dispose();
    addressInput.dispose();
  }

  Future<void> buyCrypto() async {
    final uri = Uri.parse(
        "https://widget.onramper.com?color=1CA7EC&defaultCrypto=BNB&apiKey=pk_prod_1yymCXaDroiCHCWFy8PYGYelkmJSQF2xjv3fvq2hejY0");
    if (!await launchUrl(uri)) {
      throw Exception('Could not launch $uri');
    }
  }
}

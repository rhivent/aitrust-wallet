import 'package:flutter/material.dart';
import 'package:flutter_screen_lock/flutter_screen_lock.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:walletdapps/app/controllers/global_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/password_controller.dart';

class PasswordView extends GetView<PasswordController> {
  PasswordView({Key? key}) : super(key: key);
  final globalC = Get.find<GlobalController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Unlock Password'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(
              height: Get.height,
              child: ScreenLock(
                correctString: GetStorage().read('passcode') ?? "1111",
                onUnlocked: () {
                  if (globalC.deeplinkApp.toString().isNotEmpty) {
                    if (globalC.deeplinkApp
                        .toString()
                        .contains("apps/receive")) {
                      Get.offAllNamed(Routes.RECEIVETX);
                    } else if (globalC.deeplinkApp
                        .toString()
                        .contains("apps/sendtoken")) {
                      final addressToken = globalC.deeplinkApp
                          .toString()
                          .split("sendtoken?addressToken=")[1];
                      Get.offAllNamed(Routes.SENDTX, arguments: {
                        "token_address": addressToken,
                        "name": "Token",
                        "symbol": "Token",
                        "logo": "",
                        "thumbnail": "",
                        "decimals": 18,
                        "balance": 0,
                      });
                    } else {
                      Get.offAllNamed(Routes.HOME);
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:get/get.dart';

import '../controllers/detail_nft_controller.dart';

class DetailNftBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailNftController>(
      () => DetailNftController(),
    );
  }
}

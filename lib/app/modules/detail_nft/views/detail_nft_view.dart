import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/detail_nft_controller.dart';

class DetailNftView extends GetView<DetailNftController> {
  final walletC = Get.find<WalletController>();
  DetailNftView({Key? key}) : super(key: key);
  /*
    {
      "token_address": "0xe96c3a55cbf804c98027621285e07fbc5f6f7474",
      "token_id": "4",
      "owner_of": "0xbcefc6b0134c34d4de6d3956b028dfa2f3c79fdb",
      "block_number": "24150337",
      "block_number_minted": "24150337",
      "token_hash": "52108cbb9dffb9c5449effcf4034f025",
      "amount": "45",
      "contract_type": "ERC1155",
      "name": null,
      "symbol": null,
      "token_uri": "https://bafyreifvvlan6n2pzocco5cgwljqq7v6wd5r7efbvna6c5kl6ga3nlwxmy.ipfs.nftstorage.link/metadata.json",
      "metadata": "{\"name\":\"Pitbull Tickets\",\"description\":\"Ticket live music PITBULL ALL NIGHT PARTY\",\"category\":\"ticket\",\"nfttype\":\"erc1155\",\"amountnft\":50,\"image\":\"ipfs://bafybeib3mkexemck4hyap4xhqsqabqbnp7mngpvkd364uck6xvkbyondqu/pitbulltickets.jpg\"}",
      "last_token_uri_sync": "2022-10-30T08:30:43.247Z",
      "last_metadata_sync": "2022-11-14T07:16:55.408Z",
      "minter_address": "ERC1155 tokens don't have a single minter",
      "normalized_metadata": {
        "name": "Pitbull Tickets",
        "description": "Ticket live music PITBULL ALL NIGHT PARTY",
        "animation_url": null,
        "external_link": null,
        "image": "ipfs://bafybeib3mkexemck4hyap4xhqsqabqbnp7mngpvkd364uck6xvkbyondqu/pitbulltickets.jpg",
        "attributes": []
      }
    },
  */
  @override
  Widget build(BuildContext context) {
    var arguments =
        Get.arguments == null ? {} : Get.arguments as Map<String, dynamic>;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Detail NFT"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 200,
            child: Stack(
              fit: StackFit.passthrough,
              children: [
                Positioned(
                  top: 0,
                  left: 0,
                  width: Get.width,
                  child: Container(
                    child: arguments["normalized_metadata"]["image"] != null
                        ? GestureDetector(
                            onTap: () {
                              Get.defaultDialog(
                                title: "",
                                content: arguments["normalized_metadata"]
                                            ["image"] !=
                                        null
                                    ? Image.network(
                                        arguments["normalized_metadata"]
                                                ["image"]
                                            .replaceAll("ipfs.infura.io",
                                                "nftstorage.link")
                                            .replaceAll("ipfs://",
                                                "http://nftstorage.link/ipfs/"),
                                        fit: BoxFit.contain,
                                      )
                                    : const Center(
                                        child: Text("No Image"),
                                      ),
                              );
                            },
                            child: Image.network(
                              arguments["normalized_metadata"]["image"]
                                  .replaceAll(
                                      "ipfs.infura.io", "nftstorage.link")
                                  .replaceAll("ipfs://",
                                      "http://nftstorage.link/ipfs/"),
                              fit: BoxFit.cover,
                            ),
                          )
                        : const Center(
                            child: Text("No Image"),
                          ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Colors.lightBlue,
                          Colors.blueGrey,
                        ],
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    margin: const EdgeInsets.only(top: 10, left: 10),
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      arguments['contract_type'],
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: const LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Colors.lightBlue,
                          Colors.blueGrey,
                        ],
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    margin: const EdgeInsets.only(top: 10, right: 10),
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.filter_outlined,
                          color: Colors.white,
                          size: 16,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          arguments['amount'],
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "Token Address",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    await Clipboard.setData(
                      ClipboardData(
                        text: arguments['token_address'],
                      ),
                    );
                    Get.snackbar(
                      "Success",
                      "Success copied token address",
                    );
                  },
                  child: Text(
                    arguments['token_address'],
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                const Text(
                  "Owner address",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    await Clipboard.setData(
                      ClipboardData(
                        text: arguments['owner_of'],
                      ),
                    );
                    Get.snackbar(
                      "Success",
                      "Success copied owner address",
                    );
                  },
                  child: Text(
                    arguments['owner_of'],
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          onPressed: () =>
                              Get.toNamed(Routes.SENDNFT, arguments: arguments),
                          icon: const Icon(
                            Icons.upload,
                          ),
                        ),
                        const Text("Send"),
                      ],
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      children: [
                        IconButton(
                          onPressed: () => Get.toNamed(
                            Routes.RECEIVETX,
                          ),
                          icon: const Icon(
                            Icons.get_app_rounded,
                          ),
                        ),
                        const Text("Receive"),
                      ],
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      children: [
                        IconButton(
                          onPressed: () => Get.toNamed(Routes.BROWSERS,
                              arguments:
                                  "${walletC.selectedBlockchain.explorerUrl}block/${arguments['block_number']}"),
                          icon: const Icon(
                            Icons.explore,
                          ),
                        ),
                        const Text("Explore"),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                SizedBox(
                  width: double.infinity,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Metadata",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            const Text(
                              "Name",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              arguments['normalized_metadata']['name'],
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            const Text(
                              "Description",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              arguments['normalized_metadata']['description'],
                            ),
                          ]),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

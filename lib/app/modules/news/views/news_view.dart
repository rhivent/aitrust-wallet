import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/widgets/bottomnav_widget.dart';

import '../controllers/news_controller.dart';

class NewsView extends GetView<NewsController> {
  const NewsView({Key? key}) : super(key: key);

  bool isNegativePriceChange(dynamic dt) {
    if (dt >= 0) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar() {
      return AppBar(
        title: const Text('Discover'),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () => controller.refreshData(),
            icon: const Icon(Icons.refresh),
          )
        ],
      );
    }

    double heightDevice = MediaQuery.of(context).size.height;
    double paddingTopDefault = MediaQuery.of(context).padding.top;
    double paddingBottomDefault = MediaQuery.of(context).padding.bottom;

    double heightContent = heightDevice -
        paddingTopDefault -
        paddingBottomDefault -
        appBar().preferredSize.height -
        Get.bottomBarHeight;

    return Scaffold(
      appBar: appBar(),
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            DefaultTabController(
              length: 2, // length of tabs
              initialIndex: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const TabBar(
                    labelColor: Colors.blue,
                    unselectedLabelColor: Colors.grey,
                    tabs: [
                      Tab(text: 'News'),
                      Tab(text: 'Markets'),
                    ],
                  ),
                  Container(
                    height: heightContent,
                    decoration: const BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.grey, width: 0.5),
                      ),
                    ),
                    child: TabBarView(
                      children: [
                        Obx(
                          () => controller.loadingDt.value
                              ? const LoadingComp()
                              : RecentNews(controller: controller),
                        ),
                        Obx(
                          () => controller.loadingMarket.value
                              ? const LoadingComp()
                              : ListView.separated(
                                  padding: const EdgeInsets.only(bottom: 200),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 10,
                                        horizontal: 8,
                                      ),
                                      child: GestureDetector(
                                        onTap: () => Get.toNamed(
                                            Routes.BROWSERS,
                                            arguments:
                                                "https://www.coingecko.com/id/koin_koin/${controller.topCrypto[index]['id']}"),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  width: 45,
                                                  height: 45,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: NetworkImage(
                                                        controller.topCrypto[
                                                                index]['image']
                                                            ['large'],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 8,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "${controller.topCrypto[index]['name']}",
                                                      style: const TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                    Text(
                                                      "${controller.topCrypto[index]['symbol'].toUpperCase()}/USD",
                                                      style: const TextStyle(
                                                        fontSize: 13,
                                                        color: Colors.blueGrey,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "\$${controller.topCrypto[index]['market_data']['current_price']['usd']}",
                                                  style: const TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                Text(
                                                  "${controller.topCrypto[index]['market_data']['price_change_percentage_24h']}%",
                                                  maxLines: 1,
                                                  softWrap: true,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    color:
                                                        isNegativePriceChange(
                                                      controller.topCrypto[
                                                                  index]
                                                              ['market_data'][
                                                          'price_change_percentage_24h'],
                                                    )
                                                            ? Colors.red
                                                            : Colors.green,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: controller.topCrypto.length,
                                  separatorBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      height: 1,
                                      color:
                                          const Color.fromARGB(31, 81, 81, 81),
                                    );
                                  },
                                ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavComp(),
    );
  }
}

class RecentNews extends StatelessWidget {
  const RecentNews({
    super.key,
    required this.controller,
  });

  final NewsController controller;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(bottom: 200),
      itemBuilder: (BuildContext context, int index) {
        var items = controller.topNews[index];
        return Container(
          margin: const EdgeInsets.all(
            5,
          ),
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          width: 300,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.7),
                spreadRadius: 1,
                blurRadius: 1,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: GestureDetector(
            onTap: () => Get.toNamed(
              Routes.BROWSERS,
              arguments: items['url'],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          "${items['source']['name']}",
                          style: const TextStyle(
                            fontSize: 10.0,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      Text(
                        "${items['title']}",
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        "${items['description']}",
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  width: 80,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue[300],
                    image: DecorationImage(
                      colorFilter: const ColorFilter.mode(
                          Color.fromARGB(221, 237, 234, 234), BlendMode.darken),
                      opacity: 1.0,
                      image: NetworkImage(
                          "${items['urlToImage'] ?? 'https://picsum.photos/300/300.jpg'}"),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
      itemCount: controller.topNews.length,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          height: 1,
        );
      },
    );
  }
}

class LoadingComp extends StatelessWidget {
  const LoadingComp({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}

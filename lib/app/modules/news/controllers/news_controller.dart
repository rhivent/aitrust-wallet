import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsController extends GetxController {
  final topCrypto = [].obs;
  final topNews = [].obs;
  final loadingDt = true.obs;
  final loadingMarket = true.obs;

  late Timer mytimer;

  Future<void> getTopCrypto() async {
    try {
      Uri apiUrl = Uri.parse("https://api.coingecko.com/api/v3/coins/");
      HttpClient http = HttpClient();
      http.autoUncompress = true;
      final HttpClientRequest request = await http.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final data = json.decode(content) as List<dynamic>;
      topCrypto.value = data;
      loadingMarket.toggle();
    } catch (e) {
      loadingMarket.toggle();
      throw e.toString();
    }
  }

  Future<void> getTopNews() async {
    try {
      var dateNow = DateTime.now();
      var fromYesterday = dateNow.subtract(const Duration(days: 1));
      Uri apiUrl = Uri.parse(
          "https://newsapi.org/v2/everything?q=cryptocurrency&from=${fromYesterday.year}-${fromYesterday.month}-${fromYesterday.day}&sortBy=publishedAt&apiKey=c046efa0f0fd453ab01aaf7a3c44676e");
      HttpClient http = HttpClient();
      http.autoUncompress = true;
      final HttpClientRequest request = await http.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final decodedDt = json.decode(content);
      final List data = decodedDt['articles'];
      //  EXAMPLE data ==>
      //  {
      // 	"source": {
      // 		"id": null,
      // 		"name": "International Business Times"
      // 	},
      // 	"author": "Nica Osorio",
      // 	"title": "Tether Flaunts $700M Q4 2022 Net Profit Despite Troublesome Crypto Winter",
      // 	"description": "\"We are proud of how Tether has continued to be a driving force in rebuilding trust within the crypto industry, and we are determined to continue to set a positive example for our peers and competitors alike,\" Tether CTO Paolo Ardoino said.",
      // 	"url": "https://www.ibtimes.com/tether-flaunts-700m-q4-2022-net-profit-despite-troublesome-crypto-winter-3666618",
      // 	"urlToImage": "https://d.ibtimes.com/en/full/3505029/smartphone-tether-logo-placed-displayed-us-dollars-this-illustration-taken-may-12-2022.jpg",
      // 	"publishedAt": "2023-02-10T08:50:40Z",
      // 	"content": "KEY POINTS\r\n<ul><li>Tether's December 31, 2022 attestation report dated was completed by BDO Italia</li><li>The report recorded around $67 billion in consolidated assets and excess reserves of around… [+3351 chars]"
      // },
      topNews.value = data;
      loadingDt.toggle();
    } catch (e) {
      loadingDt.toggle();
      throw e.toString();
    }
  }

  Future<void> browseUrl(String uri) async {
    if (!await launchUrl(Uri.parse(uri))) {
      throw Exception('Could not launch $uri');
    }
  }

  @override
  void onInit() {
    mytimer = Timer.periodic(const Duration(seconds: 180), (timer) {
      loadingMarket.toggle();
      getTopCrypto();
    });
    getTopCrypto();
    getTopNews();
    super.onInit();
  }

  void refreshData() {
    loadingDt.toggle();
    loadingMarket.toggle();
    getTopCrypto();
    getTopNews();
  }

  @override
  void onClose() {
    mytimer.cancel(); //to end timer
    // TODO: implement onClose
    super.onClose();
  }
}

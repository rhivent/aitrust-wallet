import 'package:get/get.dart';

import '../controllers/receivetx_controller.dart';

class ReceivetxBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ReceivetxController>(
      () => ReceivetxController(),
    );
  }
}

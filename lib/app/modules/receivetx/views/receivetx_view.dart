import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';

import '../controllers/receivetx_controller.dart';

class ReceivetxView extends GetView<ReceivetxController> {
  final walletC = Get.find<WalletController>();

  ReceivetxView({super.key});

  @override
  Widget build(BuildContext context) {
    String address = walletC.datawal['pub'] ?? "no-address";

    return Scaffold(
      appBar: AppBar(
        title: const Text('Receive'),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 100,
          ),
          TextButton(
            onPressed: () async {
              await Clipboard.setData(ClipboardData(text: address));
              Get.snackbar("Success", "Success copied !");
            },
            child: Text(address),
          ),
          Center(
            child: QrImage(
              data: address,
              version: QrVersions.auto,
              size: 320,
              backgroundColor: Colors.white,
              embeddedImage: const AssetImage('assets/img/logoOnly.png'),
              embeddedImageStyle: QrEmbeddedImageStyle(
                size: const Size(40, 40),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

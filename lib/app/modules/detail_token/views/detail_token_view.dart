import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/detail_token_controller.dart';

class DetailTokenView extends GetView<DetailTokenController> {
  const DetailTokenView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var arguments =
        Get.arguments == null ? {} : Get.arguments as Map<String, dynamic>;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(arguments["name"]),
        foregroundColor: Colors.black,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Obx(
            () => controller.loadingData.value
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView(
                    children: [
                      Column(
                        children: [
                          CircleAvatar(
                            backgroundImage: NetworkImage(
                                controller.tokenDetails != null
                                    ? controller.tokenDetails["image"]["large"]
                                    : "https://picsum.photos/536/354"),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Text(
                            "${balances(arguments['balance'], Get.arguments['decimals'])} ${arguments['symbol']}",
                            style: const TextStyle(
                              fontSize: 36,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "\$${controller.tokenDetails != null ? controller.tokenDetails['market_data']['current_price']['usd'] : '0'}",
                                style: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              Text(
                                controller.tokenDetails != null
                                    ? "${controller.tokenDetails['market_data']['price_change_percentage_24h'].toString()}%"
                                    : '0%',
                                style: TextStyle(
                                  color: controller.tokenDetails != null
                                      ? (controller.tokenDetails['market_data'][
                                                  'price_change_percentage_24h'] >
                                              0
                                          ? Colors.green
                                          : Colors.red)
                                      : Colors.green,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Column(
                                children: [
                                  IconButton(
                                    onPressed: () => Get.toNamed(Routes.SENDTX,
                                        arguments: arguments),
                                    icon:
                                        const Icon(Icons.file_upload_outlined),
                                  ),
                                  const Text("Send"),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(
                                    onPressed: () =>
                                        Get.toNamed(Routes.RECEIVETX),
                                    icon: const Icon(
                                        Icons.file_download_outlined),
                                  ),
                                  const Text("Receive"),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(
                                    onPressed: () {},
                                    icon: const Icon(Icons.swap_horiz),
                                  ),
                                  const Text("Swap"),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.3),
                          borderRadius: BorderRadius.circular(26),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 6),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: ElevatedButton(
                                  onPressed: () {
                                    controller.typeTab.value = "history";
                                  },
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50.0),
                                    ),
                                    elevation: 0,
                                    backgroundColor:
                                        controller.typeTab.value == "history"
                                            ? Colors.blue
                                            : Colors.transparent,
                                  ),
                                  child: const Text('History'),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Obx(
                                  () => ElevatedButton(
                                    onPressed: () {
                                      controller.typeTab.value = "details";
                                    },
                                    style: ElevatedButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50.0),
                                      ),
                                      elevation: 0,
                                      backgroundColor:
                                          controller.typeTab.value == "history"
                                              ? Colors.transparent
                                              : Colors.blue,
                                    ),
                                    child: const Text('Info'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Obx(
                        () => controller.loadingData.value
                            ? const Center(
                                child: CircularProgressIndicator(),
                              )
                            : tokenRender(controller.typeTab.value),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  Widget tokenRender(String typeData) {
    switch (typeData) {
      case "history":
        return controller.historyToken.isNotEmpty
            ? Column(
                children: [
                  ...controller.historyToken.map((x) {
                    var dtItem = x as Map<String, dynamic>;
                    return GestureDetector(
                      onTap: () =>
                          controller.browseUrl(dtItem["transaction_hash"]),
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                      "From: ${dtItem['from_address'].substring(0, 5)}..."),
                                  Text(dtItem['block_timestamp']),
                                ],
                              ),
                              Text(
                                  "To: ${dtItem['to_address'].substring(0, 5)}..."),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text(
                                    "Amount",
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    "${balances(dtItem['value'].toString(), Get.arguments['decimals'])} ${Get.arguments == null ? '' : Get.arguments['symbol']}",
                                    style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              )
            : const Center(child: Text("No Data"));
      default:
        return controller.tokenDetails != null
            ? Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Name: ${controller.tokenDetails['name']}",
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      await Clipboard.setData(ClipboardData(
                          text: controller.tokenDetails['contract_address']));
                      Get.snackbar("Success", "Success copied !");
                    },
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "Contract Address: ${controller.tokenDetails['contract_address'].substring(0, 8)}...",
                        style: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                    "Capitalization",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Market Cap"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['market_cap']['usd']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Fully Diluted Valuation"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['fully_diluted_valuation']['usd']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Total Volume"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['total_volume']['usd']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Circulating Supply"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['circulating_supply']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Max Supply"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['max_supply']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Total Supply"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['total_supply']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("All Time High"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['ath']['usd']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("All Time Low"),
                      Text(
                          "\$${controller.tokenDetails['market_data']['atl']['usd']}"),
                    ],
                  ),
                  const Text(
                    "Score",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Market Cap Rank"),
                      Text(
                          "${controller.tokenDetails['market_cap_rank'] ?? '-'}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("CoinGecko Rank"),
                      Text("${controller.tokenDetails['coingecko_rank']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Coingecko Score"),
                      Text("${controller.tokenDetails['coingecko_score']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Developer Score"),
                      Text("${controller.tokenDetails['developer_score']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Community Score"),
                      Text("${controller.tokenDetails['community_score']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Liquidity Score"),
                      Text("${controller.tokenDetails['liquidity_score']}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Public Interest Score"),
                      Text(
                          "${controller.tokenDetails['public_interest_score']}"),
                    ],
                  ),
                  const Text(
                    "About",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    controller.tokenDetails['description']['en'],
                    softWrap: true,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                ],
              )
            : const Center(
                child: Text("No Data"),
              );
    }
  }

  String balances(String dt, dynamic decimals) {
    var bigdt = BigInt.from(double.parse(dt)) / BigInt.from(pow(10, decimals));
    var firstVal = bigdt.toString().split('.')[0];

    var secondVal = bigdt.toString().split('.')[1];
    if (secondVal.length > 4) {
      return "$firstVal.${secondVal.substring(0, 4)}";
    }
    return "$firstVal.$secondVal";
  }
}

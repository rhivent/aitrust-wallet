import 'package:get/get.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';
import 'dart:io';
// import 'dart:math';

import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/config/constantsData.dart';

class DetailTokenController extends GetxController {
  final walletC = Get.find<WalletController>();

  final loadingData = false.obs;
  final typeTab = "history".obs;
  var tokenDetails;
  var historyToken = [].obs;

  @override
  void onInit() {
    loadingData.value = true;
    getHistoryTokenInBlockchain();
    super.onInit();
  }

  Future<void> getTokenDetails() async {
    try {
      HttpClient client = HttpClient();
      client.autoUncompress = true;

      Uri apiAssetPlatform =
          Uri.parse('https://api.coingecko.com/api/v3/asset_platforms');

      final HttpClientRequest requestAssetPlatform =
          await client.getUrl(apiAssetPlatform);
      requestAssetPlatform.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      final HttpClientResponse responseAssetPlatform =
          await requestAssetPlatform.close();

      final String contentAssetPlatform =
          await responseAssetPlatform.transform(utf8.decoder).join();
      final List dataAssetPlatform = json.decode(
          contentAssetPlatform); // [{id: theta, chain_identifier: 361, name: Theta, shortname: }, ]
      final getDataByCurrentBlock = dataAssetPlatform.firstWhere((element) =>
          element["chain_identifier"] ==
          walletC.selectedChainId.value
              .toInt()); // {id: theta, chain_identifier: 361, name: Theta, shortname: },

      // Get detail token by aaset platform and address
      Uri apiUrl = Uri.parse(
          'https://api.coingecko.com/api/v3/coins/${getDataByCurrentBlock["id"]}/contract/${Get.arguments["token_address"]}');
      final HttpClientRequest request = await client.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final data = json.decode(content);
      tokenDetails = data;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<void> getHistoryTokenInBlockchain() async {
    try {
      HttpClient client = HttpClient();
      client.autoUncompress = true;
      Uri apiHistoryToken = Uri.parse(
          'https://deep-index.moralis.io/api/v2/erc20/${Get.arguments["token_address"]}/transfers?chain=0x${walletC.selectedChainId.value.toRadixString(16)}');

      final HttpClientRequest requestHistoryToken =
          await client.getUrl(apiHistoryToken);
      requestHistoryToken.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      requestHistoryToken.headers
          .set("X-API-Key", ConstantsClass.moralisAPIKey);

      final HttpClientResponse responseHistoryToken =
          await requestHistoryToken.close();

      final String contentHistoryToken =
          await responseHistoryToken.transform(utf8.decoder).join();
      final dataHistoryToken =
          json.decode(contentHistoryToken) as Map<String, dynamic>;
      final result = dataHistoryToken['result'];
      historyToken.value = result;
      // print(result);
      await getTokenDetails();
      loadingData.toggle();
    } catch (e) {
      loadingData.toggle();
      throw e.toString();
    }
  }

  Future<void> browseUrl(String dt) async {
    var uri = "${walletC.selectedBlockchain.explorerUrl}tx/$dt";
    Get.toNamed(
      Routes.BROWSERS,
      arguments: uri,
    );
    // if (!await launchUrl(Uri.parse(uri))) {
    //   throw Exception('Could not launch $uri');
    // }
  }
}

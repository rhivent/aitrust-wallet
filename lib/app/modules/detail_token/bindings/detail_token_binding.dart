import 'package:get/get.dart';

import '../controllers/detail_token_controller.dart';

class DetailTokenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailTokenController>(
      () => DetailTokenController(),
    );
  }
}

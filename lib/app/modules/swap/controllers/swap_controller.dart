import 'dart:io';
import "dart:math";
import 'dart:convert';

import 'package:get/get.dart';
// import "package:http/http.dart";
import 'package:flutter/widgets.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';

class SwapController extends GetxController {
  final walletC = Get.find<WalletController>();
  TextEditingController originC = TextEditingController(text: "");
  TextEditingController resultC = TextEditingController(text: "");
  final originToken = "BNB".obs;
  final resultToken = "BUSD".obs;
  final isOriginExactToken = true.obs;
  final priceSwap = "".obs;

  final balanceOrigin = "".obs;
  final balanceResult = "".obs;

  final loadingSwap = false.obs;
  final loadingPriceSwap = false.obs;

  List<String> listToken = <String>['BNB', 'BUSD'];
  var listTokenByChain = {
    "56": ['BNB', 'BUSD', "DIGIS"],
    "97": ['BNB', 'BUSD'],
  };
  final routerAddress = {
    "56": "0x10ED43C718714eb63d5aA57B78B54704E256024E",
    "97": "0xD99D1c33F9fC3444f8101754aBC46c52416550D1",
  };
  get _addressToken => {
        "56": {
          "BNB": "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c",
          "BUSD": "0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56",
          "DIGIS": "0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A",
        },
        "97": {
          "BNB": "0xae13d989dac2f0debff460ac112a837c89baa7cd",
          "BUSD": "0xaB1a4d4f1D656d2450692D237fdD6C7f9146e814",
        },
      };

  get addressToken => _addressToken;

  @override
  void onReady() async {
    try {
      var tokens = _addressToken[walletC.selectedChainId.value.toString()]
          as Map<String, String>;
      // check balance user
      balanceOrigin.value = walletC.balance;
      var resultBlc =
          await walletC.balanceToken(tokens[resultToken.value]!); // BUSD;
      balanceResult.value = (resultBlc / BigInt.from(pow(10, 18))).toString();
      checkEquality();
      resultC.text = "";
    } catch (e) {
      Get.snackbar("Error onReady", e.toString());
    }
    super.onReady();
  }

  @override
  void onClose() {
    originC.dispose();
    resultC.dispose();
    super.onClose();
  }

  void balanceInputPercent(double percent) {
    var calculateDt = double.parse(balanceOrigin.value) * percent;
    originC.text = calculateDt.toString();
    var resultTokendt = calculateDt * double.parse(priceSwap.value);
    resultC.text = resultTokendt.toString();
  }

  void exchangeBtn() {
    var tempToken = resultToken.value;
    resultToken.value = originToken.value;
    originToken.value = tempToken;

    var tempBalance = balanceOrigin.value;
    balanceOrigin.value = balanceResult.value;
    balanceResult.value = tempBalance;
    originC.clear();
    resultC.clear();
    if (resultToken.value == "BNB") {
      isOriginExactToken.value = false;
    } else {
      isOriginExactToken.value = true;
    }
    checkEquality();
  }

  Future<void> checkEquality() async {
    try {
      loadingPriceSwap.toggle();
      var apiUrl = Uri.parse(
          "https://bsc.api.0x.org/swap/v1/quote?buyToken=${resultToken.value}&sellToken=${originToken.value}&sellAmount=1000000000000000000&excludedSources=Belt,DODO,DODO_V2,Ellipsis,Mooniswap,MultiHop,Nerve,Smoothy,ApeSwap,CafeSwap,CheeseSwap,JulSwap,LiquidityProvider,WaultSwap,WOOFi,Synapse");

      HttpClient client = HttpClient();
      client.autoUncompress = true;

      final HttpClientRequest request = await client.getUrl(apiUrl);
      request.headers.set(
          HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
      // request.headers.set("X-API-Key", ConstantsClass.moralisAPIKey);
      final HttpClientResponse response = await request.close();

      final String content = await response.transform(utf8.decoder).join();
      final data = json.decode(content);
      if (data['code'] == 100) {
        priceSwap.value = "0";
        throw "Could not find token !";
      }
      /*
      {"chainId":56,"price":"0.003041382961456999","guaranteedPrice":"0.003010969131842429","estimatedPriceImpact":"0.4366","to":"0xdef1c0ded9bec7f1a1670819833240f027b25eff","data":"0xc43c9ef600000000000000000000000000000000000000000000000000000000000000800000000000000000000000000000000000000000000000000de0b6b3a7640000000000000000000000000000000000000000000000000000000ab275e1887b7d00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000000000000000000000e9e7cea3dedca5984780bafc599bd69add087d56000000000000000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee869584cd000000000000000000000000100000000000000000000000000000000000001100000000000000000000000000000000000000000000009dc68432b864210007","value":"0","gas":"111000","estimatedGas":"111000","estimatedGasForRouter":111000,"gasPrice":"5000000000","protocolFee":"0","minimumProtocolFee":"0","buyTokenAddress":"0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee","sellTokenAddress":"0xe9e7cea3dedca5984780bafc599bd69add087d56","buyAmount":"3041382961456999","sellAmount":"1000000000000000000","sources":[{"name":"BakerySwap","proportion":"0"},{"name":"Belt","proportion":"0"},{"name":"DODO","proportion":"0"},{"name":"DODO_V2","proportion":"0"},{"name":"Ellipsis","proportion":"0"},{"name":"Mooniswap","proportion":"0"},{"name":"Nerve","proportion":"0"},{"name":"Synapse","proportion":"0"},{"name":"PancakeSwap","proportion":"1"},{"name":"PancakeSwap_V2","proportion":"0"},{"name":"SushiSwap","proportion":"0"},{"name":"ApeSwap","proportion":"0"},{"name":"WaultSwap","proportion":"0"},{"name":"FirebirdOneSwap","proportion":"0"},{"name":"ACryptoS","proportion":"0"},{"name":"KyberDMM","proportion":"0"},{"name":"BiSwap","proportion":"0"},{"name":"MDex","proportion":"0"},{"name":"KnightSwap","proportion":"0"},{"name":"WOOFi","proportion":"0"}],"orders":[{"type":0,"source":"PancakeSwap","makerToken":"0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c","takerToken":"0xe9e7cea3dedca5984780bafc599bd69add087d56","makerAmount":"3041382961456999","takerAmount":"1000000000000000000","fillData":{"tokenAddressPath":["0xe9e7cea3dedca5984780bafc599bd69add087d56","0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c"],"router":"0x05ff2b0db69458a0750badebc4f9e13add608c7f"},"fill":{"input":"1000000000000000000","output":"3041382961456999","adjustedOutput":"2591382961456999","gas":90000}}],"allowanceTarget":"0xdef1c0ded9bec7f1a1670819833240f027b25eff","decodedUniqueId":"9dc68432b8-1679884295","sellTokenToEthRate":"327.362587420510071365","buyTokenToEthRate":"1","expectedSlippage":null}
      */
      priceSwap.value = data['price'];
      loadingPriceSwap.toggle();
    } catch (e) {
      loadingPriceSwap.toggle();
      Get.snackbar("ERROR checkEquality", e.toString());
    }
  }

  Future<void> swapFunc() async {
    try {
      var amountOrigin = "";

      if (originC.text.length > 15) {
        amountOrigin = originC.text.substring(0, 15);
      } else {
        amountOrigin = originC.text;
      }

      loadingSwap.toggle();
      if (originC.text == "") {
        throw "Required main balance to swap";
      }
      if (double.parse(originC.text) > double.parse(balanceOrigin.value)) {
        throw "Insufficient balance !";
      }

      var tokens = _addressToken[walletC.selectedChainId.value.toString()]
          as Map<String, String>;

      // print(isOriginExactToken.value);
      // print(routerAddress[walletC.selectedChainId.value.toString()]!);
      // print(double.parse(amountOrigin));
      // print(tokens[originToken.value]!);
      // print(tokens[resultToken.value]!);

      var hash = await walletC.swapWallet(
        isOriginExactToken.value,
        routerAddress[walletC.selectedChainId.value.toString()]!,
        double.parse(amountOrigin),
        tokens[originToken.value]!, // busd
        tokens[resultToken.value]!, // wbnb
      );

      loadingSwap.toggle();
      Get.snackbar("Success", "Tx on hash: $hash");
      Get.offAllNamed(Routes.HOME);
    } catch (e) {
      loadingSwap.toggle();
      Get.snackbar("ERROR SWAP", e.toString());
    }
  }
}

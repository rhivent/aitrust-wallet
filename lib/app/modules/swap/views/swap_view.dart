import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/swap_controller.dart';

class SwapView extends GetView<SwapController> {
  const SwapView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SWAP'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              children: [
                Stack(
                  children: [
                    Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(8),
                              topLeft: Radius.circular(8),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Obx(
                                  () => TextField(
                                    autocorrect: false,
                                    autofocus: true,
                                    readOnly: controller.loadingPriceSwap.value,
                                    controller: controller.originC,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      labelText: 'You Pay',
                                      hintText: "0.0",
                                      helperText:
                                          "Balance ${controller.balanceOrigin}",
                                    ),
                                    onChanged: (text) {
                                      final valSelection =
                                          TextSelection.collapsed(
                                              offset: controller
                                                  .originC.text.length);
                                      controller.originC.text = text;
                                      controller.originC.selection =
                                          valSelection;
                                      var resultTokendt = double.parse(text) *
                                          double.parse(
                                              controller.priceSwap.value);
                                      controller.resultC.text =
                                          resultTokendt.toString();
                                    },
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Obx(
                                () => DropdownButton<String>(
                                  value: controller.originToken.value,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  elevation: 16,
                                  style: const TextStyle(color: Colors.blue),
                                  underline: Container(
                                    height: 0,
                                    color: Colors.blueAccent,
                                  ),
                                  onChanged: (String? value) {
                                    if (value == controller.resultToken.value) {
                                      controller.exchangeBtn();
                                    }
                                    // This is called when the user selects an item.
                                    controller.originToken.value = value!;
                                  },
                                  items: controller.listTokenByChain[controller
                                          .walletC.selectedChainId
                                          .toString()]!
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: const BorderRadius.only(
                              bottomRight: Radius.circular(8),
                              bottomLeft: Radius.circular(8),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Obx(
                                  () => TextField(
                                    autocorrect: false,
                                    controller: controller.resultC,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      labelText: "You Received",
                                      hintText: "0.0",
                                      helperText:
                                          "Balance ${controller.balanceResult}",
                                    ),
                                    keyboardType: TextInputType.number,
                                    onChanged: (text) {
                                      controller.resultC.text = text;
                                    },
                                    readOnly: true,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Obx(
                                () => DropdownButton<String>(
                                  value: controller.resultToken.value,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  elevation: 16,
                                  style: const TextStyle(color: Colors.blue),
                                  underline: Container(
                                    height: 0,
                                    color: Colors.blueAccent,
                                  ),
                                  onChanged: (String? value) {
                                    if (value == controller.originToken.value) {
                                      controller.exchangeBtn();
                                    }
                                    // This is called when the user selects an item.
                                    // dropdownValue = value!;
                                    controller.resultToken.value = value!;
                                  },
                                  items: controller.listTokenByChain[controller
                                          .walletC.selectedChainId
                                          .toString()]!
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: 75,
                      right: 10,
                      child: CircleAvatar(
                        backgroundColor: const Color(0xffffffff),
                        child: IconButton(
                          onPressed: controller.exchangeBtn,
                          icon: const Icon(
                            Icons.swap_vert,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      onTap: () => controller.balanceInputPercent(0.25),
                      child: const Percentage(percent: 25),
                    ),
                    InkWell(
                      onTap: () => controller.balanceInputPercent(0.5),
                      child: const Percentage(percent: 50),
                    ),
                    InkWell(
                      onTap: () => controller.balanceInputPercent(0.75),
                      child: const Percentage(percent: 75),
                    ),
                    InkWell(
                      onTap: () => controller.balanceInputPercent(1),
                      child: const Percentage(percent: 100),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Obx(
                  () => controller.loadingPriceSwap.value
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : Center(
                          child: Text(
                              "1 ${controller.originToken.value} ~= ${controller.priceSwap.value} ${controller.resultToken.value}"),
                        ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Obx(
                  () => controller.loadingSwap.value == true
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: ElevatedButton(
                            onPressed: controller.swapFunc,
                            style: ElevatedButton.styleFrom(
                              shape: const StadiumBorder(),
                              padding: const EdgeInsets.symmetric(
                                vertical: 15.0,
                              ),
                              backgroundColor: Colors.blue,
                            ),
                            child: const Text("SWAP"),
                          ),
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Percentage extends StatelessWidget {
  final int percent;

  const Percentage({
    super.key,
    required this.percent,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: Colors.blue.shade200,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        "$percent%",
        style: const TextStyle(
          color: Colors.black,
        ),
      ),
    );
  }
}

// const List<String> list = <String>['One', 'Two', 'Three', 'Four'];

// class DropdownButtonToken extends StatefulWidget {
//   const DropdownButtonToken({super.key});

//   @override
//   State<DropdownButtonToken> createState() => _DropdownButtonTokenState();
// }

// class _DropdownButtonTokenState extends State<DropdownButtonToken> {
//   String dropdownValue = list.first;

//   @override
//   Widget build(BuildContext context) {
//     return DropdownButton<String>(
//       value: dropdownValue,
//       icon: const Icon(Icons.arrow_downward),
//       elevation: 16,
//       style: const TextStyle(color: Colors.deepPurple),
//       underline: Container(
//         height: 2,
//         color: Colors.deepPurpleAccent,
//       ),
//       onChanged: (String? value) {
//         // This is called when the user selects an item.
//         setState(() {
//           dropdownValue = value!;
//         });
//       },
//       items: list.map<DropdownMenuItem<String>>((String value) {
//         return DropdownMenuItem<String>(
//           value: value,
//           child: Text(value),
//         );
//       }).toList(),
//     );
//   }
// }

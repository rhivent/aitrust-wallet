import 'package:flutter/material.dart';
import 'package:flutter_screen_lock/flutter_screen_lock.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:walletdapps/app/routes/app_pages.dart';

import '../controllers/setup_password_controller.dart';

class SetupPasswordView extends GetView<SetupPasswordController> {
  const SetupPasswordView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Set up Password'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(
              height: Get.height,
              child: ScreenLock.create(
                onCancelled: () {},
                inputController: controller.passcodeC,
                onConfirmed: (matchedText) async {
                  await GetStorage().write("passcode", matchedText);
                  Get.offAllNamed(Routes.HOME);
                },
                footer: TextButton(
                  onPressed: () {
                    // Release the confirmation state and return to the initial input state.
                    controller.passcodeC.unsetConfirmed();
                  },
                  child: const Text('Reset input'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

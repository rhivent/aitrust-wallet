// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

// class AppColor {
//   static const Color primary = Color(0xFF1D9BF0);
//   static const Color secondary = Color(0xFF253341);
//   static const Color success = Color(0xFF00CB6A);
//   static const Color danger = Color(0xFFF26666);
//   static const Color dark = Color(0xFF15202B);
//   static const Color grayLight = Color(0xFFAAB8C2);
//   static const Color white = Color(0xFFF5F8FA);
// }

// class AppTextStyle {
//   static TextStyle appName = GoogleFonts.inter(
//     fontSize: 24.0,
//     fontWeight: FontWeight.w400,
//     color: AppColor.white,
//   );
//   static TextStyle heading1 = GoogleFonts.montserrat(
//     fontSize: 34.0,
//     fontWeight: FontWeight.w700,
//     color: AppColor.white,
//   );
//   static TextStyle heading2 = GoogleFonts.montserrat(
//     fontSize: 24.0,
//     fontWeight: FontWeight.w700,
//   );
//   static TextStyle body = GoogleFonts.montserrat(
//     fontSize: 16.0,
//     fontWeight: FontWeight.w500,
//   );
//   static TextStyle button = GoogleFonts.montserrat(
//     fontSize: 14.0,
//     fontWeight: FontWeight.w700,
//   );
//   static TextStyle caption = GoogleFonts.montserrat(
//     fontSize: 12.0,
//     fontWeight: FontWeight.w400,
//   );
//   static TextStyle smallCaption = GoogleFonts.montserrat(
//     fontSize: 10.0,
//     fontWeight: FontWeight.w300,
//   );
// }

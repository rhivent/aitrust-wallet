import 'dart:convert';

class ConstantsClass {
  static const String stabilityAPIKey =
      "sk-78sz9T8l6iDQarMaInPD2olY17nSc1CCJtosY0z1S2M3dyJR";
  static const String moralisAPIKey =
      'GHaOnEKSePHWYlGDFmJ4iAquUFUbN9G9dsb5fzD9wPrrpVWJi9AeaRuWr2rk5QX8';
  static const String nftStorage =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDFkZTk5NjkyNDAyODVCQjdCN2QzMzFmZTljNTQ3MUY1MmU4MmExQTUiLCJpc3MiOiJuZnQtc3RvcmFnZSIsImlhdCI6MTY2NDk0OTAyOTIwNSwibmFtZSI6Ik5GVCBNYXJrZXRwbGFjZSBNb2JpbGUifQ.QXL7Jp9aN6Jtm18ag8ICafRfb4lyD6mZWyYrJmE0xJw';

  static const listStringNetwork = [
    {
      "name": "BSC testnet",
      "value": "97",
    },
    {
      "name": "BSC Mainnet",
      "value": "56",
    },
    {
      "name": "Mumbai Polygon",
      "value": "80001",
    },
    {
      "name": "Polygon",
      "value": "137",
    },
    {
      "name": "ETH Mainnet",
      "value": "1",
    },
    {
      "name": "Goerli",
      "value": "5",
    },
  ];
  static const networkData = [
    {
      "httpsRPC":
          "https://bsc-testnet.nodereal.io/v1/e9a36765eb8a40b9bd12e680a1fd2bc5",
      "wssRPC":
          "wss://bsc-testnet.nodereal.io/v1/e9a36765eb8a40b9bd12e680a1fd2bc5",
      "chainId": 97,
      "logo": "https://cryptologos.cc/logos/bnb-bnb-logo.png",
      "nativeCoin": "BNB",
      "chainIdHex": "0x61",
      "explorerURL": "https://testnet.bscscan.com/",
      "baseAPI": "https://api-testnet.bscscan.com/api?",
      "apikKey": "",
      "tokens": [
        {"name": "BNB", "address": ""},
        {
          "name": "DIGIS",
          "address": "0x38136e005add9Db5A26d7223696368532d55442e"
        },
        {
          "name": "USDT",
          "address": "0xd922D4cD21Dc1F62D8D8AE0Db3039Cc0F36Fa83C"
        }
      ]
    },
    {
      "httpsRPC": "https://bsc-dataseed1.ninicoin.io/",
      "wssRPC": "wss://bsc-dataseed1.ninicoin.io/",
      "chainId": 56,
      "chainIdHex": "0x38",
      "logo": "https://cryptologos.cc/logos/bnb-bnb-logo.png",
      "nativeCoin": "BNB",
      "explorerURL": "https://bscscan.com/",
      "baseAPI": "https://api.bscscan.com/api?",
      "apikKey": "9MXTQH6N9TSTJ3ID4SVX5BKB61UNXPC667",
      "tokens": [
        {"name": "BNB", "address": ""},
        {
          "name": "DIGIS",
          "address": "0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A"
        },
        {
          "name": "USDT",
          "address": "0x55d398326f99059ff775485246999027b3197955"
        }
      ]
    },
    {
      "httpsRPC": "https://polygon-bor.publicnode.com",
      "wssRPC": "",
      "chainId": 137,
      "chainIdHex": "0x89",
      "logo": "https://cryptologos.cc/logos/polygon-matic-logo.png",
      "nativeCoin": "MATIC",
      "explorerURL": "https://polygonscan.com/",
      "baseAPI": "https://api.polygonscan.com/api?",
      "apikKey": "",
      "tokens": [
        {"name": "MATIC", "address": ""},
        {
          "name": "DIGIS",
          "address": "0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A"
        },
      ]
    },
    {
      "httpsRPC": "https://endpoints.omniatech.io/v1/matic/mumbai/public",
      "wssRPC": "",
      "chainId": 80001,
      "chainIdHex": "0x13881",
      "logo": "https://cryptologos.cc/logos/polygon-matic-logo.png",
      "nativeCoin": "MATIC",
      "explorerURL": "https://mumbai.polygonscan.com/",
      "baseAPI": "https://api.mumbai.polygonscan.com/api?",
      "apikKey": "",
      "tokens": [
        {"name": "MATIC", "address": ""},
        {
          "name": "DIGIS",
          "address": "0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A"
        },
      ]
    },
    {
      "httpsRPC": "https://goerli.blockpi.network/v1/rpc/public",
      "wssRPC": "",
      "chainId": 5,
      "chainIdHex": "0x5",
      "logo":
          "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/1257px-Ethereum_logo_2014.svg.png",
      "nativeCoin": "ETH",
      "explorerURL": "https://goerli.etherscan.io/",
      "baseAPI": "https://api.goerli.etherscan.io/api?",
      "apikKey": "",
      "tokens": [
        {"name": "ETH", "address": ""},
        {
          "name": "DIGIS",
          "address": "0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A"
        },
      ]
    },
    {
      "httpsRPC": "https://eth.llamarpc.com",
      "wssRPC": "",
      "chainId": 1,
      "chainIdHex": "0x1",
      "logo":
          "https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/1257px-Ethereum_logo_2014.svg.png",
      "nativeCoin": "ETH",
      "explorerURL": "https://etherscan.io/",
      "baseAPI": "https://api.etherscan.io/api?",
      "apikKey": "",
      "tokens": [
        {"name": "ETH", "address": ""},
        {
          "name": "DIGIS",
          "address": "0x0Ff81F18bCb9519ac6027c732D196945CA4D2a9A"
        },
      ]
    },
    // {
    //   "httpsRPC": "https://api.avax.network/ext/bc/C/rpc",
    //   "wssRPC": "",
    //   "chainId": 43114,
    //   "chainIdHex": "0x1",
    //   "logo": "https://docs.avax.network/img/favicon.png",
    //   "nativeCoin": "AVAX",
    //   "explorerURL": "https://avascan.info/blockchain/c/",
    //   "baseAPI": "https://api.etherscan.io/api?",
    //   "apikKey": "",
    //   "tokens": [
    //     {"name": "AVAX", "address": ""},
    //   ]
    // },
    // {
    //   "httpsRPC": "https://api.avax.network/ext/bc/C/rpc",
    //   "wssRPC": "",
    //   "chainId": 43113,
    //   "chainIdHex": "0x1",
    //   "logo": "https://docs.avax.network/img/favicon.png",
    //   "nativeCoin": "AVAX",
    //   "explorerURL": "https://avascan.info/blockchain/c/",
    //   "baseAPI": "https://api.etherscan.io/api?",
    //   "apikKey": "",
    //   "tokens": [
    //     {"name": "AVAX", "address": ""},
    //   ]
    // },
  ];
}
// To parse this JSON data, do
//
//     final blockchainData = blockchainDataFromJson(jsonString);

BlockchainData blockchainDataFromJson(String str) =>
    BlockchainData.fromJson(json.decode(str));

String blockchainDataToJson(BlockchainData data) => json.encode(data.toJson());

class BlockchainData {
  BlockchainData({
    required this.httpsRpc,
    required this.wssRpc,
    required this.chainId,
    required this.chainIdHex,
    required this.explorerUrl,
    required this.baseApi,
    required this.apikKey,
    required this.tokens,
    required this.nativeCoin,
    required this.logo,
  });

  String httpsRpc;
  String wssRpc;
  int chainId;
  String chainIdHex;
  String explorerUrl;
  String baseApi;
  String apikKey;
  String nativeCoin;
  String logo;
  List<Token> tokens;

  factory BlockchainData.fromJson(Map<String, dynamic> json) => BlockchainData(
        httpsRpc: json["httpsRPC"],
        wssRpc: json["wssRPC"],
        chainId: json["chainId"],
        chainIdHex: json["chainIdHex"],
        explorerUrl: json["explorerURL"],
        baseApi: json["baseAPI"],
        nativeCoin: json["nativeCoin"],
        logo: json["logo"],
        apikKey: json["apikKey"],
        tokens: List<Token>.from(json["tokens"].map((x) => Token.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "httpsRPC": httpsRpc,
        "wssRPC": wssRpc,
        "chainId": chainId,
        "chainIdHex": chainIdHex,
        "explorerURL": explorerUrl,
        "baseAPI": baseApi,
        "apikKey": apikKey,
        "nativeCoin": nativeCoin,
        "logo": logo,
        "tokens": List<dynamic>.from(tokens.map((x) => x.toJson())),
      };

  static List<BlockchainData> fromJsonList(List list) {
    if (list.isEmpty) return List<BlockchainData>.empty();
    return list.map((item) => BlockchainData.fromJson(item)).toList();
  }
}

class Token {
  Token({
    required this.name,
    required this.address,
  });

  String name;
  String address;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
        name: json["name"],
        address: json["address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
      };

  static List<Token> fromJsonList(List list) {
    if (list.isEmpty) return List<Token>.empty();
    return list.map((item) => Token.fromJson(item)).toList();
  }
}

class Assetmanager {
  static const baseAssetUrl = 'assets';
  static const logoOnly = "$baseAssetUrl/img/logoOnly.png";
  static const logoHz = "$baseAssetUrl/img/logoHz.png";
  static const logoVrtcl = "$baseAssetUrl/img/logoVrtcl.png";
  static const successNotif = "$baseAssetUrl/img/success_notif.svg";
}

TokensMoralis tokensMoralisFromJson(String str) =>
    TokensMoralis.fromJson(json.decode(str));
String tokensMoralisToJson(TokensMoralis data) => json.encode(data.toJson());

class TokensMoralis {
  TokensMoralis({
    required this.tokenAddress,
    required this.name,
    required this.symbol,
    this.logo,
    this.thumbnail,
    required this.decimals,
    required this.balance,
  });

  String tokenAddress;
  String name;
  String symbol;
  dynamic logo;
  dynamic thumbnail;
  int decimals;
  String balance;

  factory TokensMoralis.fromJson(Map<String, dynamic> json) => TokensMoralis(
        tokenAddress: json["token_address"],
        name: json["name"],
        symbol: json["symbol"],
        logo: json["logo"],
        thumbnail: json["thumbnail"],
        decimals: json["decimals"],
        balance: json["balance"],
      );

  Map<String, dynamic> toJson() => {
        "token_address": tokenAddress,
        "name": name,
        "symbol": symbol,
        "logo": logo,
        "thumbnail": thumbnail,
        "decimals": decimals,
        "balance": balance,
      };
  static List<TokensMoralis> fromJsonList(List list) {
    if (list.isEmpty) return List<TokensMoralis>.empty();
    return list.map((item) => TokensMoralis.fromJson(item)).toList();
  }
}

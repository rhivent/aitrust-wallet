import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/global_controller.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/config/constantsData.dart';

class DrawerComponent extends StatelessWidget {
  final walletC = Get.find<WalletController>();
  final globalC = Get.find<GlobalController>();

  DrawerComponent({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          SizedBox(
            height: 80,
            child: DrawerHeader(
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.all(0.0),
              decoration: const BoxDecoration(
                color: Colors.blue,
              ),
              child: Image.asset(
                Assetmanager.logoHz,
                fit: BoxFit.contain,
              ),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.wallet),
            title: const Text("Wallet Key"),
            onTap: () {
              Get.defaultDialog(
                title: "Key",
                content: Column(
                  children: [
                    const Text("Seed Phrases"),
                    TextButton(
                      child: Text("${walletC.datawal['seed'] ?? '-'}"),
                      onPressed: () {
                        Clipboard.setData(
                          ClipboardData(text: walletC.datawal['seed'] ?? '-'),
                        );
                        Get.snackbar("Success", "Success Copied !");
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text("Private Key"),
                    TextButton(
                      child: Text("${walletC.datawal['pv'] ?? '-'}"),
                      onPressed: () {
                        Clipboard.setData(
                          ClipboardData(text: walletC.datawal['pv'] ?? '-'),
                        );
                        Get.snackbar("Success", "Success Copied !");
                      },
                    ),
                  ],
                ),
              );
            },
          ),
          Obx(
            () => ListTile(
              leading: globalC.theme
                  ? const Icon(Icons.sunny)
                  : const Icon(Icons.nightlight),
              title: Text(globalC.theme ? 'Get Light' : 'Get Dark'),
              onTap: () {
                globalC.settheme(!globalC.theme);
              },
            ),
          ),
          ListTile(
            leading: const Icon(Icons.history),
            title: const Text('History'),
            onTap: () => Get.toNamed(
              Routes.BROWSERS,
              arguments:
                  "${walletC.selectedBlockchain.explorerUrl}address/${walletC.datawal['pub']}",
            ),
          ),
          ListTile(
            leading: const Icon(Icons.location_city),
            title: const Text('About'),
            onTap: () => Get.toNamed(
              Routes.BROWSERS,
              arguments: "https://aitrust-web3.vercel.app/",
            ),
          ),
          ListTile(
            leading: const Icon(Icons.help_outline),
            title: const Text('Help'),
            onTap: () => Get.toNamed(Routes.CHATGPT),
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Log Out'),
            onTap: () {
              Get.back();
              Get.defaultDialog(
                title: "DELETE STORAGE",
                onCancel: () {},
                content: const Text(
                  "Are you sure ?",
                  style: TextStyle(color: Colors.red),
                ),
                onConfirm: () async {
                  await walletC.cleardt();
                },
                confirmTextColor: Colors.white,
              );
            },
          ),
        ],
      ),
    );
  }
}

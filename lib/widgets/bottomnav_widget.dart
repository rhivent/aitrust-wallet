import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:walletdapps/app/controllers/global_controller.dart';
import 'package:walletdapps/app/routes/app_pages.dart';
import 'package:walletdapps/config/constantsData.dart';

class BottomNavComp extends StatelessWidget {
  final globalC = Get.find<GlobalController>();

  BottomNavComp({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => BottomNavigationBar(
        elevation: 0,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        unselectedLabelStyle: const TextStyle(color: Colors.grey),
        selectedLabelStyle: const TextStyle(color: Colors.blue),
        currentIndex: globalC.navIndex.value,
        onTap: (val) {
          switch (val) {
            case 0:
              Get.offAllNamed(Routes.LESSON);
              break;
            case 1:
              Get.offAllNamed(Routes.NEWS);
              break;
            case 3:
              Get.offAllNamed(Routes.CHATGPT);
              break;
            case 4:
              Get.offAllNamed(Routes.BROWSERS);
              break;
            default:
              Get.offAllNamed(Routes.HOME);
              break;
          }
          globalC.setnavIndex(val);
        },
        items: <BottomNavigationBarItem>[
          const BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'Lesson',
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.manage_search_sharp),
            label: 'Discover',
          ),
          BottomNavigationBarItem(
            icon: SizedBox(
              height: 24,
              child: Image.asset(
                Assetmanager.logoOnly,
                fit: BoxFit.cover,
              ),
            ),
            label: 'AiTrust',
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.chat_rounded),
            label: 'Chat GPT',
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.screen_search_desktop_outlined),
            label: 'Browser',
          ),
        ],
      ),
    );
  }
}

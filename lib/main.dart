import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:walletdapps/app/controllers/global_controller.dart';
import 'package:walletdapps/app/controllers/wallet_controller.dart';
import 'app/routes/app_pages.dart';

Future main() async {
  await GetStorage.init();
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  final globalC = Get.put(GlobalController());
  final walletC = Get.put(WalletController());

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: globalC.theme ? ThemeData.dark() : ThemeData.light(),
        title: "Wallet",
        initialRoute: (GetStorage().read('passcode') == null)
            ? AppPages.INITIAL
            : Routes.PASSWORD,
        // initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
      ),
    );
  }
}

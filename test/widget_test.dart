// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:flutter_test/flutter_test.dart';
import 'dart:math';

// import 'package:walletdapps/contracts/Erc20.g.dart';
import 'package:web3dart/web3dart.dart';

void main() async {
  // var dt = DateTime.now().add(const Duration(days: 365)).millisecondsSinceEpoch;
  // print(dt);
  // print(dt / 1000);
  // print(BigInt.from(dt / 1000));
  // print(
  //   BigInt.from(
  //     0.0001 * pow(10, 18),
  //   ),
  // );

  EtherAmount.fromUnitAndValue(
    EtherUnit.wei,
    BigInt.from(0.0001 * pow(10, 18)),
  );

  var dt = EtherAmount.fromUnitAndValue(
    EtherUnit.gwei,
    BigInt.from(1),
  );
// 5000000000
// 1000000000000000000
  print(dt);

  // testWidgets('Counter increments smoke test', (WidgetTester tester) async {
  //   // Build our app and trigger a frame.
  //   await tester.pumpWidget(MyApp());

  //   // Verify that our counter starts at 0.
  //   expect(find.text('0'), findsOneWidget);
  //   expect(find.text('1'), findsNothing);

  //   // Tap the '+' icon and trigger a frame.
  //   await tester.tap(find.byIcon(Icons.add));
  //   await tester.pump();

  //   // Verify that our counter has incremented.
  //   expect(find.text('0'), findsNothing);
  //   expect(find.text('1'), findsOneWidget);
  // });

  // Web3Client ethClient = Web3Client(
  //     "https://bsc-testnet.nodereal.io/v1/e9a36765eb8a40b9bd12e680a1fd2bc5",
  //     Client(), socketConnector: () {
  //   return IOWebSocketChannel.connect(
  //           "wss://bsc-testnet.nodereal.io/v1/e9a36765eb8a40b9bd12e680a1fd2bc5")
  //       .cast<String>();
  // });

  // var constClass = BlockchainData.fromJsonList(ConstantsClass.networkData);
  // var dt = constClass.firstWhere((element) => element.chainId == 97);
  // print(dt.httpsRpc);
  // print(dt.tokens);
  // var token = dt.tokens.firstWhere((e) => e.name == "DIGIS");
  // print(token.address);

  // var cred = EthPrivateKey.fromHex(
  //     "d539da63c50ccd3b7018d00e0fa5678cef1213f594f2b316bb2efa124b532f62");
  // await allowance("0x8C1A5b5d41CcA3C4068bCDbbE3fBe770b8B2AEdc",
  //     '0x38136e005add9Db5A26d7223696368532d55442e', 97, ethClient);
  // await checkBalance("0x8C1A5b5d41CcA3C4068bCDbbE3fBe770b8B2AEdc", ethClient);
  // await transfer(
  //   recipientAddress: "0xBceFc6B0134c34d4De6d3956B028dFa2f3C79fdb",
  //   web3C: ethClient,
  //   amount: BigInt.from(0.001 * pow(10, 18)),
  //   privateKey:
  //       "d539da63c50ccd3b7018d00e0fa5678cef1213f594f2b316bb2efa124b532f62",
  // );

  // Uri apiUrl = Uri.parse(
  //     "https://newsapi.org/v2/everything?q=crypto&from=2023-01-10&sortBy=publishedAt&apiKey=c046efa0f0fd453ab01aaf7a3c44676e");
  // HttpClient http = HttpClient();
  // http.autoUncompress = true;
  // final HttpClientRequest request = await http.getUrl(apiUrl);
  // request.headers
  //     .set(HttpHeaders.contentTypeHeader, "application/json; charset=UTF-8");
  // final HttpClientResponse response = await request.close();

  // final String content = await response.transform(utf8.decoder).join();
  // final decodedDt = json.decode(content);
  // final List data = decodedDt['articles'];
  // print(data);
}

// Future<Erc20> loadContract(String address, int chainId, ethClient) async {
//   return Erc20(
//     address: EthereumAddress.fromHex(address),
//     client: ethClient,
//     chainId: chainId,
//   );
// }

// Future<BigInt> allowance(
//     String address, String tokenaddress, int chainId, ethClient) async {
//   try {
//     var _contractERC20 = await loadContract(tokenaddress, chainId, ethClient);
//     BigInt allowance = await _contractERC20.balanceOf(
//       EthereumAddress.fromHex(address),
//     );
//     print(allowance.toString());
//     var dr = allowance / BigInt.from(10).pow(18);
//     print(dr - 1);
//     print(EtherAmount.fromUnitAndValue(EtherUnit.wei, allowance));
//     print(EtherAmount.inWei(allowance));
//     return allowance;
//   } catch (e) {
//     throw e.toString();
//   }
// }

// Future<void> checkBalance(String address, Web3Client web3C) async {
//   try {
//     // credentials = EthPrivateKey.fromHex(datawal['pv']);
//     // print(address.hexEip55);
//     var bl = await web3C.getBalance(EthereumAddress.fromHex(address));
//     var value = bl.getValueInUnit(EtherUnit.ether).toString();
//     print(value);
//   } catch (e) {
//     if (kDebugMode) {
//       print('masuk sini');
//       print(e);
//     }
//   }
// }

Future<String> transfer({
  required String privateKey,
  required String recipientAddress,
  required BigInt amount,
  Web3Client? web3C,
  int chainId = 97,
}) async {
  EthPrivateKey credentials = EthPrivateKey.fromHex(privateKey);
  try {
    String transfer = await web3C!.sendTransaction(
      credentials,
      Transaction(
        to: EthereumAddress.fromHex(recipientAddress),
        value: EtherAmount.inWei(amount),
      ),
      chainId: chainId,
    );
    // print(transfer);
    return transfer;
  } catch (e) {
    throw e.toString();
  }
}

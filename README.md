# walletdapps

A new Flutter project.
reference
https://medium.com/geekculture/simple-dapp-using-flutter-and-solidity-b64f5267acf4

## API reference

https://newsapi.org/docs/endpoints/everything
https://docs.moralis.io/web3-data-api/reference/get-wallet-token-balances
https://www.coingecko.com/id/api/documentation

# API DeepLink

http://aitrust-web3.vercel.app/apps/
http://aitrust-web3.vercel.app/apps/receive
http://aitrust-web3.vercel.app/apps/sendtoken?addressToken=0x1E85bEd62FF50A46bB3De87d1114b563f35C4Cde

app://com.digiswap.aitrust/apps/
app://com.digiswap.aitrust/apps/receive
app://com.digiswap.aitrust/apps/sendtoken?addressToken=0x1E85bEd62FF50A46bB3De87d1114b563f35C4Cde

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
